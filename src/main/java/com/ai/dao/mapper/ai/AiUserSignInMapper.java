package com.ai.dao.mapper.ai;

import com.ai.dao.entity.ai.AiUserSignIn;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AiUserSignInMapper extends BaseMapper<AiUserSignIn> {
    List<AiUserSignIn> getUserSignInByUidDate(String uid, List<String> dateList);
}
