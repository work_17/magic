package com.ai.dao.mapper.ai;

import com.ai.dao.entity.FeedbackSuggest;
import tk.mybatis.mapper.common.BaseMapper;

public interface FeedbackSuggestMapper extends BaseMapper<FeedbackSuggest> {
}
