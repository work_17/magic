package com.ai.dao.mapper.ai;

import com.ai.dao.entity.ai.AiUserOperatingRecord;
import tk.mybatis.mapper.common.BaseMapper;

public interface AiUserOperatingRecordMapper  extends BaseMapper<AiUserOperatingRecord> {
}
