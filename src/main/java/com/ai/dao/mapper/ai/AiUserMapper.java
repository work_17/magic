package com.ai.dao.mapper.ai;

import com.ai.dao.entity.ai.AiUser;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AiUserMapper extends BaseMapper<AiUser> {

    List<AiUser> getCurDateUser();
}
