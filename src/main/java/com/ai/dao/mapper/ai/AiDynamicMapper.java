package com.ai.dao.mapper.ai;

import com.ai.dao.entity.ai.AiDynamic;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AiDynamicMapper extends BaseMapper<AiDynamic> {

    List<AiDynamic> getListDesc(String optionType,String title);

    AiDynamic getAiDynamicById(Long id);

}
