package com.ai.dao.mapper.ai;

import com.ai.dao.entity.ai.AiSensitiveWordRecord;
import tk.mybatis.mapper.common.BaseMapper;

public interface AiSensitiveWordRecordMapper extends BaseMapper<AiSensitiveWordRecord> {

}
