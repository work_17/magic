package com.ai.dao.mapper.ai;

import com.ai.dao.entity.ai.AiUserAccessRecord;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface AiUserAccessRecordMapper extends BaseMapper<AiUserAccessRecord> {

    List<AiUserAccessRecord> getUserCallOnHistory(AiUserAccessRecord aiUserAccessRecord);

    List<AiUserAccessRecord> getUserCallOnHistoryByType(AiUserAccessRecord aiUserAccessRecord);
}
