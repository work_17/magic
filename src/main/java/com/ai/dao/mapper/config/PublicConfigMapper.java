package com.ai.dao.mapper.config;

import com.ai.dao.entity.PublicConfig;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface PublicConfigMapper extends BaseMapper<PublicConfig> {
    List<String> getModuleList();
}
