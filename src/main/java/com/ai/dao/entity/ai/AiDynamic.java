package com.ai.dao.entity.ai;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.util.Date;

@Data
public class AiDynamic {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private String title;
    private String optionType;
    private String summary;
    private String content;
    private String author;
    private String source;
    private Long dateTimestamp;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
