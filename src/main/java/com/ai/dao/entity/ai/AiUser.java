package com.ai.dao.entity.ai;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.util.Date;

@Data
public class AiUser {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private String uid;
    private String openId;
    private String name;
    private String cardList;
    /**
     * 微信昵称
     */
    private String nickName;
    /**
     * 微信头像
     */
    private String avatarUrl;
    /**
     * 微信语言
     */
    private String language;
    private String phone;
    private String headPortrait;
    private Integer integral;
    private Integer vipType;
    private Date vipStartDate;
    private Date vipEndDate;
    private String shareUid;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
