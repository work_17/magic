package com.ai.dao.entity.ai;

import lombok.Data;

import java.util.Date;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/11 10:40
 * @description：
 * @version:
 */
@Data
public class AiUserSignIn {
    private Long id;
    private String uid;
    private String dateStr;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
