package com.ai.dao.entity.ai;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.util.Date;

@Data
public class AiUserOperatingRecord {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private String uid;
    private Integer type;
    private Integer count;
    private String dateStr;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
