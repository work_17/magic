package com.ai.dao.entity.ai;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.util.Date;

@Data
public class AiUserAccessRecord {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private String uid;
    private String ip;
    private String equipment;
    private String addresse;
    private Date dateTime;
    private String record;
    private String requestUrl;
    private String optionType;
    private String request;
    private String response;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
