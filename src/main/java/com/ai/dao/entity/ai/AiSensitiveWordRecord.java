package com.ai.dao.entity.ai;

import lombok.Data;

import java.util.Date;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/10 19:51
 * @description：
 * @version:
 */
@Data
public class AiSensitiveWordRecord {
    private Long id;
    private String uid;
    private String content;
    private String optionType;
    private String requestUrl;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
