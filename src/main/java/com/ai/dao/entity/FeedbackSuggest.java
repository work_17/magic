package com.ai.dao.entity;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.util.Date;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/27 14:46
 * @description：
 * @version:
 */
@Data
public class FeedbackSuggest {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private Long parentId;
    private String uid;
    private String content;
    private String contact;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
