package com.ai.dao.entity;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.util.Date;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:07
 * @description：
 * @version:
 */
@Data
public class PublicConfig {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private String module;
    private String configKey;
    private String value;
    private String configDesc;
    private Integer isDelete;
    private Date updateTime;
    private Date createTime;
}
