package com.ai;

import com.ai.sevice.config.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Slf4j
@Component
public class Construct {

    @Autowired
    private ConfigService configService;

    @PostConstruct
    public void doConstruct(){
        try {
            log.info("服务初始化开始...");
            configService.initConfig();
            log.info("服务初始化完成...");
        }catch (Exception e){
            log.warn("服务初始化异常...");
            log.error(e.getMessage(),e);
        }
    }
}
