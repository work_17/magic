package com.ai.web;

import com.ai.aspect.RequestLog;
import com.ai.aspect.ResponseLog;
import com.ai.sevice.ai.UserService;
import com.ai.sevice.ai.request.FeedbackRequest;
import com.ai.sevice.ai.request.LoginRequest;
import com.ai.sevice.ai.request.ShareRequest;
import com.ai.sevice.ai.response.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "user")
@CrossOrigin(origins = "*",maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 分享
     * @param request
     * @return
     */
    @PostMapping(value = "share")
    public ShareResponse share(@RequestBody ShareRequest request){
        return userService.share(request);
    }

    /**
     * 获取分享信息
     * @return
     */
    @GetMapping(value = "getShareInfo")
    public GetShareInfoResponse getShareInfo(){
        return userService.getShareInfo();
    }

    /**
     * 根据微信code获取用户信息
     * @param wxCode
     * @return
     */
    @GetMapping(value = "getUserByWXCode")
    public GetUserByWXCodeResponse getUserByWXCode(@RequestParam String wxCode){
        return userService.getUserByWXCode(wxCode);
    }

    /**
     * 根据uid获取用户信息
     * @param uid
     * @return
     */
    @GetMapping(value = "getUserByUid")
    public GetUserByUidResponse getUserByUid(@RequestParam String uid){
        return userService.getUserByUid(uid);
    }

    /**
     * 根据openid获取用户信息
     * @param openId
     * @return
     */
    @GetMapping(value = "getUserByOpenId")
    public GetUserByOpenIdResponse getUserByOpenId(@RequestParam String openId){
        return userService.getUserByOpenId(openId);
    }

    /**
     * login接口
     * @param request
     * @return
     */
    @PostMapping(value = "login")
    public LoginResponse login(@RequestBody LoginRequest request){
        return userService.login(request);
    }

    /**
     * 反馈和建议
     * @param request
     * @return
     */
    @PostMapping(value = "feedback")
    public FeedbackResponse feedback(@RequestBody FeedbackRequest request){
        return userService.feedback(request);
    }

    /**
     * @param type 1.使用说明,2.关于我们
     * @return
     */
    @GetMapping(value = "about")
    public AboutResponse about(Integer type){
        return userService.about(type);
    }

    /**
     * 签到接口
     * @param uid
     * @return
     */
    @GetMapping(value = "signIn")
    public SignInResponse signIn(@RequestParam String uid){
        return userService.signIn(uid);
    }

    /**
     * 获取福利状态
     * @param uid
     * @return
     */
    @GetMapping(value = "getWelfareStatus")
    public GetWelfareStatusResponse getWelfareStatus(@RequestHeader(value = "uid", defaultValue = "") String uid){
        return userService.getWelfareStatus(uid);
    }

    /**
     * 领取福利
     * @param uid
     * @return
     */
    @GetMapping(value = "receiveWelfare")
    public ReceiveWelfareResponse receiveWelfare(@RequestHeader(value = "uid", defaultValue = "") String uid){
        return userService.receiveWelfare(uid);
    }

    /**
     * 获取当天新用户
     * @return
     */
    @GetMapping(value = "getCurDateUser")
    public GetCurDateUserResponse getCurDateUser(){
        return userService.getCurDateUser();
    }


}
