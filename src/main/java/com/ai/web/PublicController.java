package com.ai.web;

import com.ai.sevice.PublicService;
import com.ai.sevice.publics.ContinuousLimitPoolResponse;
import com.ai.sevice.publics.HotStockResponse;
import com.ai.sevice.publics.StockMoveResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 测试Controller
 * @author ：shengwenyang
 * @date ：Created in 2023/4/12 16:48
 * @description：
 * @version:
 */
@Slf4j
@RestController
@RequestMapping(value = "public")
@CrossOrigin(origins = "*",maxAge = 3600)
public class PublicController {

    @Autowired
    private PublicService publicService;

    @GetMapping(value = "stockMove")
    public StockMoveResponse stockMove() {
        return publicService.stockMove();
    }

    @GetMapping(value = "hotStock")
    public HotStockResponse hotStock() {
        return publicService.hotStock();
    }

    @GetMapping(value = "continuousLimitPool")
    public ContinuousLimitPoolResponse continuousLimitPool(Integer page, Integer limit) {
        return publicService.continuousLimitPool(page,limit);
    }

}
