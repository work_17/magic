package com.ai.web;

import com.ai.sevice.admin.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/18 10:58
 * @description：
 * @version:
 */
@Slf4j
@RestController
@RequestMapping(value = "admin")
@CrossOrigin(origins = "*",maxAge = 3600)
public class AdminController {

    @Autowired
    private AdminService adminService;

    /**
     * 测试
     * @return
     */
    @GetMapping(value = "test")
    public String test(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

}
