package com.ai.web;

import com.ai.sevice.login.LoginService;
import com.ai.sevice.login.request.LoginRequest;
import com.ai.sevice.login.request.RegisterRequest;
import com.ai.sevice.login.request.SendVerificationCodeRequest;
import com.ai.sevice.login.request.ValidateTokenRequest;
import com.ai.sevice.login.response.LoginResponse;
import com.ai.sevice.login.response.RegisterResponse;
import com.ai.sevice.login.response.SendVerificationCodeResponse;
import com.ai.sevice.login.response.ValidateTokenResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "login")
@CrossOrigin(origins = "*",maxAge = 3600)
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 发送验证码
     * @param request
     * @return
     */
    @PostMapping(value = "sendVerificationCode")
    public SendVerificationCodeResponse sendVerificationCode(@RequestBody SendVerificationCodeRequest request) {
        return loginService.sendVerificationCode(request);
    }

    /**
     * 注册
     * @param request
     * @return
     */
    @PostMapping(value = "register")
    public RegisterResponse register(@RequestBody RegisterRequest request) {
        return loginService.register(request);
    }

    /**
     * 登陆
     * @param request
     * @return
     */
    @PostMapping(value = "login")
    public LoginResponse login(@RequestBody LoginRequest request) {
        return loginService.login(request);
    }

    /**
     * 验证token
     * @param request
     * @return
     */
    @PostMapping(value = "validateToken")
    public ValidateTokenResponse validateToken(@RequestBody ValidateTokenRequest request) {
        return loginService.validateToken(request);
    }


}
