package com.ai.web;

import com.ai.aspect.IntegralAuth;
import com.ai.aspect.RequestLog;
import com.ai.sevice.ai.GptService;
import com.ai.sevice.ai.request.ChatroomRequest;
import com.ai.sevice.ai.request.GetAiDynamicDetailRequest;
import com.ai.sevice.ai.request.GetAiDynamicListRequest;
import com.ai.sevice.ai.request.GetUserCallOnHistoryRequest;
import com.ai.sevice.ai.response.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/18 10:58
 * @description：
 * @version:
 */
@Slf4j
@RestController
@RequestMapping(value = "gpt")
@CrossOrigin(origins = "*",maxAge = 3600)
public class GptController {

    @Autowired
    private GptService gptService;

    /**
     * ai接口
     * @param request
     * @return
     */
    @RequestLog
    @IntegralAuth
    @PostMapping(value = "ai")
    public AiResponse ai(@RequestBody ChatroomRequest request){
        AiResponse ai = new AiResponse();
        String checkReq = check(request.getContent());
        if (checkReq != null){
            ai.setContent(checkReq);
            return ai;
        }

        ai = gptService.ai(request);

        String check = check(ai.getContent());
        if (check != null){
            ai.setContent(check);
        }
        String replace = "AI语言模型";
        if (ai.getContent() != null && ai.getContent().contains(replace)) {
            String content = ai.getContent().replace(replace, "智能AI");
            ai.setContent(content);
        }
        return ai;
    }

    private String check(String content) {

        if (StringUtils.isNotBlank(content)){
            content = content.replaceAll("\\s+", "");//去掉所有空格
            content = content.toLowerCase();
            if (content.contains("gpt")
                    || content.contains("chat")
                    || content.contains("共产党")
                    || content.contains("国民党")
                    || content.contains("台湾")
                    || content.contains("台独")
                    || content.contains("open")){
                return "暂时无法回答该问题";
            }
        }
        return null;
    }

    /**
     * 聊天接口
     * @param request
     * @return
     */
    @RequestLog
    @IntegralAuth
    @PostMapping(value = "chatroom")
    public ChatroomResponse chatroom(@RequestBody ChatroomRequest request) {
        ChatroomResponse chatroom = gptService.chatroom(request);
        String check = check(chatroom.getContent());
        if (check != null){
            chatroom.setContent(check);
        }
        String replace = "AI语言模型";
        if (chatroom.getContent() != null && chatroom.getContent().contains(replace)) {
            String content = chatroom.getContent().replace(replace, "智能AI");
            chatroom.setContent(content);
        }
        return chatroom;
    }

    /**
     * 聊天接口(流式返回)
     * @param request
     * @return
     */
    @RequestLog
    @IntegralAuth
    @PostMapping(value = "chatroomFlux",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> chatroomFlux(@RequestBody ChatroomRequest request) {
        return gptService.chatroomFlux(request);
    }

    /**
     * 聊天接口(流式返回)
     * @param content
     * @return
     */
    @GetMapping(value = "chatroomFluxGet",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> chatroomFluxGet(String uid,String content) {
        return gptService.chatroomFluxGet(uid,content);
    }

    /**
     * 获取用户访问记录
     * @param request
     * @return
     */
    @PostMapping(value = "getUserCallOnHistory")
    public GetUserCallOnHistoryResponse getUserCallOnHistory(@RequestBody GetUserCallOnHistoryRequest request) {
        return gptService.getUserCallOnHistory(request);
    }

    /**
     * 删除用户访问记录
     * @return
     */
    @RequestLog
    @GetMapping(value = "deleteUserCallOnHistory")
    public DeleteUserCallOnHistoryResponse deleteUserCallOnHistory(Long id) {
        return gptService.deleteUserCallOnHistory(id);
    }

    /**
     * 获取用户访问记录详情
     * @return
     */
    @GetMapping(value = "getUserCallOnHistoryDetail")
    public GetUserCallOnHistoryDetailResponse getUserCallOnHistoryDetail(Long id) {
        return gptService.getUserCallOnHistoryDetail(id);
    }

    /**
     * 获取用户聊天记录
     * @param uid
     * @return
     */
    @GetMapping(value = "getChatHistory")
    public GetChatHistoryResponse getChatHistory(@RequestParam String uid) {
        return gptService.getChatHistory(uid);
    }

    /**
     * 清空用户聊天记录
     * @param uid
     * @return
     */
    @GetMapping(value = "clearChatHistory")
    public ClearChatHistoryResponse clearChatHistory(@RequestParam String uid) {
        return gptService.clearChatHistory(uid);
    }

    /**
     * 获取ai动态列表
     * @param request
     * @return
     */
    @PostMapping(value = "getAiDynamicList")
    public GetAiDynamicListResponse getAiDynamicList(@RequestBody GetAiDynamicListRequest request) {
        return gptService.getAiDynamicList(request);
    }

    /**
     * 获取ai动态列表
     * @param request
     * @return
     */
    @PostMapping(value = "getAiDynamicDetail")
    public GetAiDynamicDetailResponse getAiDynamicDetail(@RequestBody GetAiDynamicDetailRequest request) {
        return gptService.getAiDynamicDetail(request);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    /**
//     * ai流式接口
//     * @param content
//     * @return
//     */
//    @IntegralAuth
//    @GetMapping(value = "aiFlux",produces = "text/event-stream;charset=utf-8")
//    public Flux<String> aiFlux(@RequestParam(required = true) String content,@RequestParam String optionType){
//        return gptService.aiFlux(content);
//    }

//    /**
//     * 聊天接口(流式返回，GET请求)
//     * @param content
//     * @param optionType
//     * @return
//     */
//    @IntegralAuth
//    @GetMapping(value = "chatroomFluxGet",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
//    public Flux<String> chatroomFluxGet(@RequestParam String content,@RequestParam String optionType) {
//        ChatroomRequest request = new ChatroomRequest();
//        request.setContent(content);
//        return gptService.chatroomFlux(request);
//    }


}
