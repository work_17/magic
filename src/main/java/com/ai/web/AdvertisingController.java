package com.ai.web;

import com.ai.sevice.advertising.AdvertisingService;
import com.ai.sevice.advertising.request.IsShowAdvertisingRequest;
import com.ai.sevice.advertising.request.WatchVideoRequest;
import com.ai.sevice.advertising.response.IsShowAdvertisingResponse;
import com.ai.sevice.advertising.response.WatchVideoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "advertising")
@CrossOrigin(origins = "*",maxAge = 3600)
public class AdvertisingController {

    @Autowired
    private AdvertisingService advertisingService;

    /**
     * 是否展示广告
     * @return
     */
    @PostMapping(value = "isShowAdvertising")
    public IsShowAdvertisingResponse isShowAdvertising(@RequestHeader(value = "uid", defaultValue = "") String uid,@RequestBody(required = false) IsShowAdvertisingRequest request){
        return advertisingService.isShowAdvertising(uid,request);
    }

    /**
     * 是否可观看广告视频
     * @return
     */
    @PostMapping(value = "isWatchVideo")
    public WatchVideoResponse isWatchVideo(@RequestHeader(value = "uid", defaultValue = "") String uid){
        return advertisingService.isWatchVideo(uid);
    }

    /**
     * 观看广告视频
     * @return
     */
    @PostMapping(value = "watchVideo")
    public WatchVideoResponse watchVideo(@RequestHeader(value = "uid", defaultValue = "") String uid, @RequestBody(required = false) WatchVideoRequest request){
        return advertisingService.watchVideo(uid,request);
    }

}
