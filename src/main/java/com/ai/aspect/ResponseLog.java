package com.ai.aspect;

import java.lang.annotation.*;

/**
 * 打印返回参数
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseLog {
}
