package com.ai.aspect;

import java.lang.annotation.*;

/**
 * 积分认证注解
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})//作用于参数或方法上
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IntegralAuth {

}