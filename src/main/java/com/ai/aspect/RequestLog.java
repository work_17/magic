package com.ai.aspect;

import java.lang.annotation.*;

/**
 * 打印请求参数
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestLog {
    //是否打印日志
    boolean value() default true;
}
