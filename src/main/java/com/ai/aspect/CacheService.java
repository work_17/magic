package com.ai.aspect;

import java.lang.annotation.*;

//@Retention(RetentionPolicy.RUNTIME)
//@Target({ElementType.METHOD})
@Target({ElementType.PARAMETER, ElementType.METHOD})//作用于参数或方法上
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheService {
}
