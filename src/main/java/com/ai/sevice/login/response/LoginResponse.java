package com.ai.sevice.login.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class LoginResponse extends ParentResult {
    private String token;
}
