package com.ai.sevice.login.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/6/12 16:52
 * @description：
 * @version:
 */
@Data
public class SendVerificationCodeResponse extends ParentResult {

}
