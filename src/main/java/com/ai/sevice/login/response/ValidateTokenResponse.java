package com.ai.sevice.login.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class ValidateTokenResponse extends ParentResult {
    private Boolean result;
    private String phone;
    private String userName;
}
