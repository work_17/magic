package com.ai.sevice.login;

import com.ai.common.http.SmsClient;
import com.ai.common.utils.CommonUtil;
import com.ai.common.utils.JwtUtil;
import com.ai.sevice.login.request.LoginRequest;
import com.ai.sevice.login.request.RegisterRequest;
import com.ai.sevice.login.request.SendVerificationCodeRequest;
import com.ai.sevice.login.request.ValidateTokenRequest;
import com.ai.sevice.login.response.LoginResponse;
import com.ai.sevice.login.response.RegisterResponse;
import com.ai.sevice.login.response.SendVerificationCodeResponse;
import com.ai.sevice.login.response.ValidateTokenResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;
@Slf4j
@Service
public class LoginService {

    @Autowired
    private SmsClient smsClient;

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 发送验证码
     * @param request
     * @return
     */
    public SendVerificationCodeResponse sendVerificationCode(SendVerificationCodeRequest request) {
        SendVerificationCodeResponse response = new SendVerificationCodeResponse();
        response.setCode(1);
        if (request.getType() == null || StringUtils.isBlank(request.getPhone())){
            response.setMsg("缺少请求参数");
            return response;
        }
        if (request.getType() == 0) {
            RBucket<String> randomCodeBucket = redissonClient.getBucket(CommonUtil.getVerificationCodeKey(request.getType(),request.getPhone()));
            String randomCode = CommonUtil.generateRandomCode(6);
            log.info("sendVerificationCode -> " + request.getPhone() + " -> " + randomCode);
            boolean send = smsClient.sendRegisterCode(request.getPhone(), "124658");
            if (send) {
                response.setCode(0);
            }
            //验证码保存10分钟
            randomCodeBucket.set(randomCode, 10, TimeUnit.MINUTES);
        }
        return response;
    }

    /**
     * 注册
     * @param request
     * @return
     */
    public RegisterResponse register(RegisterRequest request) {
        RegisterResponse response = new RegisterResponse();

        return response;
    }

    /**
     * 登陆
     * @param request
     * @return
     */
    public LoginResponse login(LoginRequest request) {
        LoginResponse response = new LoginResponse();
        if (StringUtils.isBlank(request.getPhone())){
            response.setCode(1);
            response.setMsg("手机号不能为空");
            return response;
        }
        if (StringUtils.isBlank(request.getPassword())){
            response.setCode(1);
            response.setMsg("密码不能为空");
            return response;
        }
        String token = JwtUtil.generateToken(request.getPhone(),"ysw");
        response.setToken(token);
        return response;
    }

    /**
     * 验证token
     * @param request
     * @return
     */
    public ValidateTokenResponse validateToken(ValidateTokenRequest request) {
        ValidateTokenResponse response = new ValidateTokenResponse();
        if (StringUtils.isBlank(request.getToken())){
            response.setResult(false);
            response.setCode(1);
            response.setMsg("token不能为空");
            return response;
        }
        boolean result = JwtUtil.validateToken(request.getToken());
        response.setResult(result);
        if (result) {
            String phone = JwtUtil.getPhoneFromToken(request.getToken());
            response.setPhone(phone);
            String userName = JwtUtil.geUserNameFromToken(request.getToken());
            response.setUserName(userName);
        }
        return response;
    }
}
