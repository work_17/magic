package com.ai.sevice.login.request;

import lombok.Data;

@Data
public class ValidateTokenRequest {
    private String token;
}
