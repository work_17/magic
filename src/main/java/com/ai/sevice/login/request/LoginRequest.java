package com.ai.sevice.login.request;

import lombok.Data;

@Data
public class LoginRequest {
    private String phone;
    private String password;
}
