package com.ai.sevice.login.request;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/6/12 16:52
 * @description：
 * @version:
 */
@Data
public class SendVerificationCodeRequest {
    /**
     * 类型：0-注册
     */
    private Integer type;
    /**
     * 手机号
     */
    private String phone;
}
