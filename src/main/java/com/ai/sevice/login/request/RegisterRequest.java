package com.ai.sevice.login.request;

import lombok.Data;

@Data
public class RegisterRequest {
    /**
     * 手机号
     */
    private String phone;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 确认密码
     */
    private String confirmPassword;
    /**
     * 验证码
     */
    private String verificationCode;
}
