package com.ai.sevice.abc;

import lombok.Data;

import java.util.List;

@Data
public class ZuoweninfoData {
    private String title;
    private List<ZuoweninfoDataParagraph> paragraph;
}
