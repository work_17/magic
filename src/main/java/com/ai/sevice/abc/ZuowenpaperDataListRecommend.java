package com.ai.sevice.abc;

import lombok.Data;

@Data
public class ZuowenpaperDataListRecommend {
    private String id;
    private String title;
    private String text;
}
