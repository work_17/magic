package com.ai.sevice.abc;

import lombok.Data;

import java.util.List;

@Data
public class ZuowenpaperDataList {
    private String paperType;
    private Integer year;
    private List<ZuowenpaperDataListRecommend> recommend;
}
