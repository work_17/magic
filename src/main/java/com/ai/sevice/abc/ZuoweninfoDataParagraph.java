package com.ai.sevice.abc;

import lombok.Data;

@Data
public class ZuoweninfoDataParagraph {
    private String contentType;
    private String text;
}
