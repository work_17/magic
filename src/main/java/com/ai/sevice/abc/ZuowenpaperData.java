package com.ai.sevice.abc;

import lombok.Data;

import java.util.List;

@Data
public class ZuowenpaperData {
    private List<ZuowenpaperDataList> list;
}
