package com.ai.sevice.config.request;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 19:16
 * @description：
 * @version:
 */
@Data
public class GetConfigListRequest {
    private String module;
    private Boolean isCache;
}
