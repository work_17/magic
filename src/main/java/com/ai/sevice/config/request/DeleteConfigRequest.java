package com.ai.sevice.config.request;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 18:40
 * @description：
 * @version:
 */
@Data
public class DeleteConfigRequest {
    private String module;
    private String key;
}
