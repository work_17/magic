package com.ai.sevice.config.request;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:32
 * @description：
 * @version:
 */
@Data
public class GetConfigRequest {
    private String module;
    private String key;
}
