package com.ai.sevice.config.request;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:16
 * @description：
 * @version:
 */
@Data
public class AddOrUpdateConfigRequest {
    private String module = "default";
    private String key;
    private String value;
}
