package com.ai.sevice.config.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:21
 * @description：
 * @version:
 */
@Data
public class AddOrUpdateConfigResponse extends ParentResult {

}
