package com.ai.sevice.config.response.dto;

import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 19:22
 * @description：
 * @version:
 */
@Data
public class ConfigModuleDto {
    private String module;
    private List<ConfigDto> configList;
}
