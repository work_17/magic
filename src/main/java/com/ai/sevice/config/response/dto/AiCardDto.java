package com.ai.sevice.config.response.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/24 11:22
 * @description：
 * @version:
 */
@Data
public class AiCardDto {
    /**
     * id
     */
    private Long id;
    /**
     * 选项类型
     */
    private String optionType;
    /**
     * 标签类型：0普通，1热门
     */
    private Integer tagType;
    /**
     * 标题
     */
    private String title;
    /**
     * 副标题
     */
    private String subTitle;
    /**
     * 输入框提示
     */
    private String desc;
    /**
     * 图标地址
     */
    private String iconUrl;

    /**
     * 是否已锁：true-是，false-否
     */
    private Boolean isLock;
}
