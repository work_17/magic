package com.ai.sevice.config.response.dto;

import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/24 11:28
 * @description：
 * @version:
 */
@Data
public class AiCardCategoryDto {
    /**
     * 种类id
     */
    private Long id;
    /**
     * 种类名称
     */
    private String name;
    /**
     * 种类卡片
     */
    private List<AiCardDto> cardList;
}
