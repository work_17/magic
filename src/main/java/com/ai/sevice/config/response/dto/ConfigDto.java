package com.ai.sevice.config.response.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 19:23
 * @description：
 * @version:
 */
@Data
public class ConfigDto {
    private String key;
    private String value;
}
