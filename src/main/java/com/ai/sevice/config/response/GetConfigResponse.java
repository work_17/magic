package com.ai.sevice.config.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:16
 * @description：
 * @version:
 */
@Data
public class GetConfigResponse extends ParentResult {
    private String value;
}
