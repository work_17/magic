package com.ai.sevice.config.response;
import com.ai.sevice.config.response.dto.ConfigModuleDto;
import lombok.Data;

import java.util.List;


/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 19:18
 * @description：
 * @version:
 */
@Data
public class GetConfigListResponse {
    private List<ConfigModuleDto> moduleList;
}
