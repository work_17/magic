package com.ai.sevice.config.response;

import com.ai.common.ParentResult;
import com.ai.sevice.config.response.dto.AiExampleDto;
import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/24 14:25
 * @description：
 * @version:
 */
@Data
public class GetAiExampleResponse extends ParentResult {
    private List<AiExampleDto> exampleList;
}
