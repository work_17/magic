package com.ai.sevice.config.response.dto;

import lombok.Data;

@Data
public class AiExampleDto {
    private String title;
    private String describe;
    private String content;
    private String color;
    private String bgColor;
}
