package com.ai.sevice.config.response;

import com.ai.common.ParentResult;
import lombok.Data;

import java.util.List;

@Data
public class GetModuleListResponse extends ParentResult {
    List<String> moduleList;
}
