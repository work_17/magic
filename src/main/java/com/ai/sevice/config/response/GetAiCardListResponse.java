package com.ai.sevice.config.response;

import com.ai.common.ParentResult;
import com.ai.sevice.config.response.dto.AiCardCategoryDto;
import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/24 11:21
 * @description：
 * @version:
 */
@Data
public class GetAiCardListResponse extends ParentResult {

    /**
     * 首页内容
     */
    private String homeContent;
    /**
     * 首页公告
     */
    private String homeAdvice;
    /**
     * 加群图标
     */
    private String addGroupIcon;
    /**
     * 微信图片
     */
    private String wxImg;
    /**
     * 微信图片描述
     */
    private String wxImgDesc;
    /**
     * 卡片种类列表
     */
    private List<AiCardCategoryDto> cardCategoryList;

}
