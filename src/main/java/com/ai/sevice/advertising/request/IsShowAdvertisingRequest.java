package com.ai.sevice.advertising.request;

import lombok.Data;

@Data
public class IsShowAdvertisingRequest {
    private String adUnitId;
}
