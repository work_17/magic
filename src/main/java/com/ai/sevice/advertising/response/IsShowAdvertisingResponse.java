package com.ai.sevice.advertising.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class IsShowAdvertisingResponse extends ParentResult {
    /**
     * 是否展示广告
     */
    private Boolean isShowAdvertising;
}
