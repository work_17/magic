package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import com.ai.sevice.ai.response.dto.AiDynamicDto;
import com.ai.sevice.ai.response.dto.AiDynamicFilterDto;
import lombok.Data;

import java.util.List;

@Data
public class GetAiDynamicListResponse extends ParentResult {
    private List<AiDynamicFilterDto> filters;
    private Long total;
    private List<AiDynamicDto> list;
}
