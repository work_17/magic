package com.ai.sevice.ai.response.dto;

import lombok.Data;

@Data
public class AiDynamicFilterDto {
    private String optionType;
    private String optionName;

    public AiDynamicFilterDto() {
    }

    public AiDynamicFilterDto(String optionType, String optionName) {
        this.optionType = optionType;
        this.optionName = optionName;
    }
}
