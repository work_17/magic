package com.ai.sevice.ai.response.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/6/15 16:17
 * @description：
 * @version:
 */
@Data
public class GetCurDateUserDto {
    private Long id;
    private String uid;
    private String nickName;
    private Integer integral;
    private String shareUid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
