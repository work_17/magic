package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class GetUserByOpenIdResponse extends ParentResult {
    private Long id;
    private String uid;
    private String name;
    private String phone;
    private String headPortrait;
    private Integer integral;
    private Integer vipType;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date vipStartDate;
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date vipEndDate;
}
