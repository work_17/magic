package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import com.ai.sevice.ai.response.dto.SignInDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class GetUserByUidResponse extends ParentResult {
    /**
     * 用户id
     */
    private String uid;
    /**
     * 是否已登陆
     */
    private Boolean isLogin = false;
    /**
     * 姓名
     */
    private String name;
    /**
     * 电话
     */
    private String phone;
    /**
     * 头像
     */
    private String headPortrait;
    /**
     * 积分
     */
    private Integer integral;
    /**
     * 今天是否已签到
     */
    private Boolean isSignIn = false;
    /**
     * 签到列表
     */
    private List<SignInDto> signInList;

    ////////////////////////////////////////////////////
    /**
     * 会议类型：0-普通用户，1-普通会员，2-高级会员
     * TODO 会员到期后返回0
     */
    private Integer vipType;
    /**
     * 会员开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date vipStartDate;
    /**
     * 会员结束时间
     */
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date vipEndDate;

    /**
     * 分享描述
     */
    private String shareDesc;
    /**
     * 观看广告视频描述
     */
    private String watchVideoDesc;
    /**
     * 是否有福利分享
     */
    private Boolean welfareShare;
}
