package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/11 10:49
 * @description：
 * @version:
 */
@Data
public class SignInResponse extends ParentResult {
}
