package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import com.ai.sevice.ai.response.dto.GetCurDateUserDto;
import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/6/15 16:08
 * @description：
 * @version:
 */
@Data
public class GetCurDateUserResponse extends ParentResult {
    private Integer total;
    private List<GetCurDateUserDto> list;
}
