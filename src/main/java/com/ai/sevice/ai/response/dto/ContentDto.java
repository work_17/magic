package com.ai.sevice.ai.response.dto;

import lombok.Data;

@Data
public class ContentDto {
    private String content;
    private String describe;
}
