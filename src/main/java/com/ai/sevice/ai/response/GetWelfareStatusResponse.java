package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class GetWelfareStatusResponse extends ParentResult {
    /**
     * 福利分享图片
     */
    private String welfareShareImg;
    /**
     * 福利分享标题
     */
    private String welfareShareTitle;

    /**
     * 状态:0-无活动，1-可领取，2-已领取
     */
    private Integer status;
    /**
     * 可领取积分
     */
    private Integer integral;
    /**
     * 领取描述
     */
    private String integralDesc;
    /**
     * 截止时间
     */
    private String endTime;


}
