package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/26 14:03
 * @description：
 * @version:
 */
@Data
public class FeedbackResponse extends ParentResult {
}
