package com.ai.sevice.ai.response.dto;

import lombok.Data;

@Data
public class UserCallOnHistoryRequestDto {
    private ContentDto request;
}

