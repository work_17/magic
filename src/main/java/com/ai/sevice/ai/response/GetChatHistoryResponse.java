package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class GetChatHistoryResponse extends ParentResult {
    private String chatHistory;
}
