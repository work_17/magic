package com.ai.sevice.ai.response.dto;

import lombok.Data;

@Data
public class SignInDto {

    private Integer signDateNumber;

    private Boolean isSignIn = false;

    private Integer integral = 1;

    private String desc;
}
