package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import com.ai.sevice.ai.response.dto.UserCallOnHistoryDto;
import lombok.Data;

import java.util.List;

@Data
public class GetUserCallOnHistoryResponse extends ParentResult {

    private Long total;
    private List<UserCallOnHistoryDto> list;
}
