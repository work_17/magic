package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/20 18:51
 * @description：
 * @version:
 */
@Data
public class ChatroomResponse extends ParentResult {
    private String content;
}
