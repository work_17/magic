package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/26 17:27
 * @description：
 * @version:
 */
@Data
public class GetShareInfoResponse extends ParentResult {
    private String title;
    private String imageUrl;
    private String url;
}
