package com.ai.sevice.ai.response.dto;

import lombok.Data;

@Data
public class UserCallOnHistoryDto {

    /**
     * id
     */
    private Long id;
    /**
     * 用户内容
     */
    private String userContent;

    /**
     * 系统内容
     */
    private String systemContent;

    /**
     * 请求时间
     */
    private String dateTime;

}
