package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class GetUserByWXCodeResponse extends ParentResult {
    /**
     * 用户id
     */
    private String uid;
}
