package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class ShareResponse extends ParentResult {
    private String uid;
}
