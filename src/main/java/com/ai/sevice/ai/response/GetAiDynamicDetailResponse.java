package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

@Data
public class GetAiDynamicDetailResponse extends ParentResult {
    private String title;
    private String summary;
    private String optionType;
    private String optionName;
    private String content;
    private String author;
    private String dateTime;
}
