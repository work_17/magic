package com.ai.sevice.ai.response;

import com.ai.common.ParentResult;
import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/26 13:31
 * @description：
 * @version:
 */
@Data
public class GetUserCallOnHistoryDetailResponse extends ParentResult {
    /**
     * 用户内容
     */
    private String userContent;

    /**
     * 系统内容
     */
    private String systemContent;

    /**
     * 请求时间
     */
    private String dateTime;
}
