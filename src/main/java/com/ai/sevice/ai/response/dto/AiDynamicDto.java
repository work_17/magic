package com.ai.sevice.ai.response.dto;

import lombok.Data;

@Data
public class AiDynamicDto {
    private Long id;
    private String title;
    private String optionType;
    private String optionName;
    private String summary;
    private String author;
    private String dateTime;
}
