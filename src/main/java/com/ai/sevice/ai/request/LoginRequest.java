package com.ai.sevice.ai.request;

import lombok.Data;

@Data
public class LoginRequest {
    private String uid;
    private String nickName;
    private String avatarUrl;
    private String language;
}
