package com.ai.sevice.ai.request;

import lombok.Data;

@Data
public class GetAiDynamicDetailRequest {
    private Long id;
}
