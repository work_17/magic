package com.ai.sevice.ai.request;

import com.ai.sevice.ai.request.dto.GptRequestMessageDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/18 11:05
 * @description：
 * @version:
 */
@Data
public class FluxBackRequest {
    private String model;
    private BigDecimal max_tokens;
    private BigDecimal temperature;
    private BigDecimal frequency_penalty;
    private BigDecimal presence_penalty;
    private List<GptRequestMessageDto> messages;
}
