package com.ai.sevice.ai.request;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/26 14:04
 * @description：
 * @version:
 */
@Data
public class FeedbackRequest {
    private String uid;
    private String content;
    private String contact;
}
