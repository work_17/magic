package com.ai.sevice.ai.request.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/18 11:07
 * @description：
 * @version:
 */
@Data
public class GptRequestMessageDto {
    private String role;
    private String content;
}
