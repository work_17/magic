package com.ai.sevice.ai.request;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class GetUserCallOnHistoryRequest {

    private Integer pageNum;
    private Integer pageSize;
    private String uid;
    private String optionType;

}
