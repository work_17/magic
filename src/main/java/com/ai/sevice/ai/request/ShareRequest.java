package com.ai.sevice.ai.request;

import lombok.Data;

@Data
public class ShareRequest {
    private String uid;
    private String wxCode;
}
