package com.ai.sevice.ai.request;

import lombok.Data;

@Data
public class ChatroomRequest {
    /**
     * 内容
     */
    private String content;
    /**
     * 选项类型，如：chat
     */
    private String optionType;
    /**
     * 描述，如：java,c++,c语言,javaScript
     */
    private String describe;
}
