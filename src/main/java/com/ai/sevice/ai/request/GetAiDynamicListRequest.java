package com.ai.sevice.ai.request;

import lombok.Data;

@Data
public class GetAiDynamicListRequest {
    /**
     * 当前页
     */
    private Integer pageNo;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 类型
     */
    private String optionType;
    /**
     * 是否返回筛选项
     */
    private Boolean isFilter;
    /**
     * 标题
     */
    private String title;
}
