package com.ai.sevice.ai.remote;

import lombok.Data;

@Data
public class Choice {
    private Message message;
    private String finish_reason;
}
