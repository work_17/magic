package com.ai.sevice.ai.remote;

import lombok.Data;
import java.util.List;

@Data
public class CompletionsResponse {
    private Integer code;
    private String message;
    private String id;
    private String object;
    private String model;
    private List<Choice> choices;
}
