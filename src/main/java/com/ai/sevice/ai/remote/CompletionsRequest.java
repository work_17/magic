package com.ai.sevice.ai.remote;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/3/5 21:44
 * @description：
 * @version:
 */
@Data
public class CompletionsRequest {
    private String model;
    private BigDecimal max_tokens;
    private BigDecimal temperature;
    private BigDecimal frequency_penalty;
    private BigDecimal presence_penalty;
    private List<Message> messages;
}
