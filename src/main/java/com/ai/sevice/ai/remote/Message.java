package com.ai.sevice.ai.remote;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/3/5 21:48
 * @description：
 * @version:
 */
@Data
public class Message {
    private String role;
    private String content;

    public Message() {
    }

    public Message(String role, String content) {
        this.role = role;
        this.content = content;
    }
}
