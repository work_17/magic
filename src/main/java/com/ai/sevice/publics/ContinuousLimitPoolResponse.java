package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ContinuousLimitPoolResponse {
    private BigDecimal status_code;
    private String status_msg;
    private ContinuousLimitPoolData data;

}
