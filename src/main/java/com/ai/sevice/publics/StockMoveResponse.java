package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockMoveResponse {
    private BigDecimal code;
    private String err_info;
    private StockMoveData data;
}
