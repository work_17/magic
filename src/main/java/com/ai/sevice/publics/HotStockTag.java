package com.ai.sevice.publics;

import lombok.Data;

import java.util.List;

@Data
public class HotStockTag {
    private List<String> concept_tag;
    private String popularity_tag;
    private HotStockLiveTag live_tag;
}
