package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class HotStockList {
    private BigDecimal order;
    private String code;
    private String name;
    private String rate;
    private BigDecimal market;
    private BigDecimal hot_rank_chg;
    private List<HotStockTag> tag;
}
