package com.ai.sevice.publics;

import lombok.Data;

import java.util.List;

@Data
public class HotStockData {
    private List<HotStockList> stock_list;
}
