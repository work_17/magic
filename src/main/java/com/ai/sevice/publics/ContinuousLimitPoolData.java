package com.ai.sevice.publics;

import lombok.Data;

import java.util.List;

@Data
public class ContinuousLimitPoolData {
    private String date;
    private String msg;
    private ContinuousLimitPoolPage page;
    private List<ContinuousLimitPoolInfo> info;
    private ContinuousLimitPoolLimitUpCount limit_up_count;
    private ContinuousLimitPoolLimitDownCount limit_down_count;
    private ContinuousLimitPoolTradeStatus trade_status;
}
