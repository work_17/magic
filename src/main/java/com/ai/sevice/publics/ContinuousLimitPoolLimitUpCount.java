package com.ai.sevice.publics;

import lombok.Data;

@Data
public class ContinuousLimitPoolLimitUpCount {
    private ContinuousLimitPoolLimitUpCountToday today;
    private ContinuousLimitPoolLimitUpCountToday yesterday;
}
