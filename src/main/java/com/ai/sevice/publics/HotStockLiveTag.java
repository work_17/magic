package com.ai.sevice.publics;

import lombok.Data;

@Data
public class HotStockLiveTag {
    private String anchor;
    private String jump_url;
}
