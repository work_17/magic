package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockMoveDataData {
    private String symbol;
    private String name;
    private BigDecimal day;
}
