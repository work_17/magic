package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ContinuousLimitPoolInfo {
    private BigDecimal open_num;
    private String last_limit_up_time;
    private String code;
    private String limit_up_type;
    private BigDecimal order_volume;
    private BigDecimal is_new;
    private BigDecimal currency_value;
    private BigDecimal market_id;
    private BigDecimal sum_market_value;
    private BigDecimal is_again_limit;
    private BigDecimal change_rate;
    private BigDecimal turnover_rate;
    private String reason_type;
    private BigDecimal order_amount;
    private String high_days;
    private String name;
    private BigDecimal high_days_value;
    private String change_tag;
    private String market_type;
    private BigDecimal latest;
}
