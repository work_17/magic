package com.ai.sevice.publics;

import lombok.Data;

@Data
public class ContinuousLimitPoolLimitDownCount {
    private ContinuousLimitPoolLimitUpCountToday today;
    private ContinuousLimitPoolLimitUpCountToday yesterday;
}
