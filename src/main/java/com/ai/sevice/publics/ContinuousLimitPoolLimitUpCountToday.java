package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ContinuousLimitPoolLimitUpCountToday {
    private BigDecimal num;
    private BigDecimal history_num;
    private BigDecimal rate;
    private BigDecimal open_num;
}
