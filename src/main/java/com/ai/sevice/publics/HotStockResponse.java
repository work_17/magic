package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class HotStockResponse {
    private BigDecimal status_code;
    private String status_msg;
    private HotStockData data;
}
