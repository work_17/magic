package com.ai.sevice.publics;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ContinuousLimitPoolPage {
    private BigDecimal limit;
    private BigDecimal total;
    private BigDecimal count;
    private BigDecimal page;
}
