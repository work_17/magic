package com.ai.sevice.publics;

import lombok.Data;

import java.util.List;

@Data
public class StockMoveData {
    private String time;
    private List<StockMoveDataData> data;
}
