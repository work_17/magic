package com.ai.sevice.publics;

import lombok.Data;

@Data
public class ContinuousLimitPoolTradeStatus {
    private String id;
    private String name;
    private String start_time;
    private String end_time;
}
