package com.ai.sevice;

import com.ai.aspect.CacheService;
import com.ai.common.http.WebClient;
import com.ai.sevice.publics.ContinuousLimitPoolResponse;
import com.ai.sevice.publics.HotStockResponse;
import com.ai.sevice.publics.StockMoveResponse;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PublicService {
    @Autowired
    private WebClient webClient;

    @CacheService()
    public StockMoveResponse stockMove() {
        String url = "https://www.longhubung.com/pubapi/stock_move_m?ajax=1";
        String result = webClient.get(url);
        if (StringUtils.isNotBlank(result)){
            return JSON.parseObject(result,StockMoveResponse.class);
        }
        return null;
    }

    @CacheService()
    public HotStockResponse hotStock() {
        String url = "https://eq.10jqka.com.cn/open/api/hot_list/v1/hot_stock/a/hour/data.txt";
        String result = webClient.get(url);
        if (StringUtils.isNotBlank(result)){
            return JSON.parseObject(result,HotStockResponse.class);
        }
        return null;
    }

    @CacheService()
    public ContinuousLimitPoolResponse continuousLimitPool(Integer page, Integer limit) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("https://data.10jqka.com.cn/dataapi/limit_up/continuous_limit_pool?");
        stringBuilder.append("field=199112,10,330329,330325,133971,133970,1968584,3475914,3541450,9001,9002,330324&filter=HS,GEM2STAR&order_field=330329&order_type=0");
        stringBuilder.append("&page=").append(page);
        stringBuilder.append("&limit=").append(limit);
        String url = stringBuilder.toString();
        String result = webClient.get(url);
        if (StringUtils.isNotBlank(result)){
            return JSON.parseObject(result,ContinuousLimitPoolResponse.class);
        }
        return null;
    }
}
