package com.ai.repository;

import com.ai.dao.entity.PublicConfig;
import com.ai.dao.mapper.config.PublicConfigMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:10
 * @description：
 * @version:
 */
@Repository
public class PublicConfigRepository {

    @Autowired
    private PublicConfigMapper publicConfigMapper;

    public int insertSelective(PublicConfig publicConfig) {
        return publicConfigMapper.insertSelective(publicConfig);
    }

    public int updateByPrimaryKeySelective(PublicConfig publicConfig) {
        return publicConfigMapper.updateByPrimaryKeySelective(publicConfig);
    }

    public List<PublicConfig> getPublicConfigList(String module) {
        PublicConfig publicConfig = new PublicConfig();
        publicConfig.setModule(module);
        publicConfig.setIsDelete(0);
        List<PublicConfig> configList = publicConfigMapper.select(publicConfig);
        return configList;
    }

    public PublicConfig getPublicConfig(String module, String key) {
        if (StringUtils.isBlank(module) || StringUtils.isBlank(key)){
            return null;
        }
        PublicConfig publicConfig = new PublicConfig();
        publicConfig.setModule(module);
        publicConfig.setConfigKey(key);
        publicConfig.setIsDelete(0);
        List<PublicConfig> configList = publicConfigMapper.select(publicConfig);
        if (CollectionUtils.isNotEmpty(configList)){
            if (configList.size() > 1) {
                throw new RuntimeException("配置异常");
            }
            return configList.get(0);
        }
        return null;
    }

    public List<String> getModuleList(){
        return publicConfigMapper.getModuleList();
    }
}
