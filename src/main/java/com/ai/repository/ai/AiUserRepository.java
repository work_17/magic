package com.ai.repository.ai;

import com.ai.common.email.EmailClient;
import com.ai.common.enums.ConfigModuleEnum;
import com.ai.common.utils.ConfigUtil;
import com.ai.dao.entity.ai.AiUser;
import com.ai.dao.mapper.ai.AiUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Slf4j
@Repository
public class AiUserRepository {
    @Autowired
    private AiUserMapper aiUserMapper;

    @Autowired
    private EmailClient emailClient;

    @Autowired
    private RedissonClient redissonClient;

    public int insertSelective(AiUser aiUser){
        int insert = aiUserMapper.insertSelective(aiUser);
//        try {
//            String feedbackToEmails = ConfigUtil.getInstance().getConfig(ConfigModuleEnum.email.getModule(), "feedbackToEmails");
//            if (StringUtils.isNotBlank(feedbackToEmails)) {
//                String[] feedbackToEmailsArray = feedbackToEmails.split(",");
//                List<String> toEmails = new ArrayList<>(feedbackToEmailsArray.length);
//                //数组转list
//                Collections.addAll(toEmails, feedbackToEmailsArray);
//                //发送邮件
//                StringBuilder stringBuilder = new StringBuilder();
//                stringBuilder.append("uid -> ").append(aiUser.getUid()).append("\r\n");
//                stringBuilder.append("openId -> ").append(aiUser.getOpenId()).append("\r\n");
//                stringBuilder.append("shareUid -> ").append(aiUser.getShareUid()).append("\r\n");
//                emailClient.send( "新增用户", stringBuilder.toString(), toEmails);
//            }
//        }catch (Exception e){
//            log.error(e.getMessage(),e);
//        }
        return insert;
    }

    public int updateByPrimaryKeySelective(AiUser aiUser){
        int update = aiUserMapper.updateByPrimaryKeySelective(aiUser);
        return update;
    }

    public AiUser getUserByUid(String uid) {
        if (StringUtils.isBlank(uid)){
            return null;
        }
        AiUser aiUser = new AiUser();
        aiUser.setUid(uid);
        aiUser.setIsDelete(0);
        List<AiUser> aiUserInfoList = aiUserMapper.select(aiUser);
        if (CollectionUtils.isNotEmpty(aiUserInfoList)){
            return aiUserInfoList.get(aiUserInfoList.size() -1);
        }
        return null;
    }

    public AiUser getUserByOpenId(String openId) {
        AiUser aiUser = new AiUser();
        aiUser.setOpenId(openId);
        aiUser.setIsDelete(0);
        List<AiUser> aiUserInfoList = aiUserMapper.select(aiUser);
        if (CollectionUtils.isNotEmpty(aiUserInfoList)){
            return aiUserInfoList.get(aiUserInfoList.size() -1);
        }
        return null;
    }

    public int updateIntegral(String uid,Integer upOrDown) {
        AiUser aiUser = new AiUser();
        aiUser.setUid(uid);
        aiUser.setIsDelete(0);
        //加锁
        RLock lock = redissonClient.getLock("updateIntegral-" + uid);
        try {
            lock.lock();
            aiUser = aiUserMapper.selectOne(aiUser);
            if (aiUser != null) {
                AiUser aiUserUpdate = new AiUser();
                aiUserUpdate.setId(aiUser.getId());
                aiUserUpdate.setIntegral(aiUser.getIntegral() + upOrDown);
                int updateResult = aiUserMapper.updateByPrimaryKeySelective(aiUserUpdate);
                return updateResult;
            }
        }finally {
            lock.unlock();
        }
        return 0;
    }

    /**
     * 获取当天新用户
     * @return
     */
    public List<AiUser> getCurDateUser() {
        return aiUserMapper.getCurDateUser();
    }
}
