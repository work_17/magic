package com.ai.repository.ai;

import com.ai.dao.entity.ai.AiDynamic;
import com.ai.dao.mapper.ai.AiDynamicMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Slf4j
@Repository
public class AiDynamicRepository {
    @Autowired
    private AiDynamicMapper aiDynamicMapper;

    public int insertSelective(AiDynamic aiDynamic){
        int insert = aiDynamicMapper.insertSelective(aiDynamic);
        return insert;
    }

    public int updateByPrimaryKeySelective(AiDynamic aiDynamic){
        int update = aiDynamicMapper.updateByPrimaryKeySelective(aiDynamic);
        return update;
    }

    public List<AiDynamic> getListDesc(String optionType,String title){
        if (StringUtils.isBlank(optionType)){
            optionType = null;
        }
        return aiDynamicMapper.getListDesc(optionType,title);
    }

    public AiDynamic getAiDynamicById(Long id){
        if (id == null){
            return null;
        }
        return aiDynamicMapper.getAiDynamicById(id);
    }

    public List<AiDynamic> select(AiDynamic aiDynamic){
        if (aiDynamic == null){
            return null;
        }
        aiDynamic.setIsDelete(0);
        return aiDynamicMapper.select(aiDynamic);
    }

}
