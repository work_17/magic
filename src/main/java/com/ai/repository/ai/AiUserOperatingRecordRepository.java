package com.ai.repository.ai;

import com.ai.dao.entity.ai.AiUserOperatingRecord;
import com.ai.dao.mapper.ai.AiUserOperatingRecordMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Slf4j
@Repository
public class AiUserOperatingRecordRepository {
    @Autowired
    private AiUserOperatingRecordMapper aiUserOperatingRecordMapper;

    public int insertSelective(AiUserOperatingRecord aiUserOperatingRecord){
        int insert = aiUserOperatingRecordMapper.insertSelective(aiUserOperatingRecord);
        return insert;
    }

    public int updateByPrimaryKeySelective(AiUserOperatingRecord aiUserOperatingRecord){
        int update = aiUserOperatingRecordMapper.updateByPrimaryKeySelective(aiUserOperatingRecord);
        return update;
    }

    public AiUserOperatingRecord getAiUserOperatingRecord(Integer type,String uid,String dataStr) {
        if (type == null || StringUtils.isBlank(uid) || StringUtils.isBlank(dataStr)){
            return null;
        }
        AiUserOperatingRecord aiUserOperatingRecord = new AiUserOperatingRecord();
        aiUserOperatingRecord.setUid(uid);
        aiUserOperatingRecord.setType(type);
        aiUserOperatingRecord.setDateStr(dataStr);
        aiUserOperatingRecord.setIsDelete(0);
        List<AiUserOperatingRecord> aiUserOperatingRecordList = aiUserOperatingRecordMapper.select(aiUserOperatingRecord);
        if (CollectionUtils.isNotEmpty(aiUserOperatingRecordList)){
            return aiUserOperatingRecordList.get(0);
        }
        return null;
    }

}
