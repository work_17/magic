package com.ai.repository.ai;

import com.ai.dao.entity.ai.AiUserSignIn;
import com.ai.dao.mapper.ai.AiUserSignInMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AiUserSignInRepository {
    @Autowired
    private AiUserSignInMapper aiUserSignInMapper;

    public List<AiUserSignIn> getUserSignInByUidDate(String uid,List<String> dateList){
        if (StringUtils.isBlank(uid) || CollectionUtils.isEmpty(dateList)){
            return null;
        }
        List<AiUserSignIn> list = aiUserSignInMapper.getUserSignInByUidDate(uid, dateList);
        return list;
    }

    public List<AiUserSignIn> select(AiUserSignIn aiUserSignIn){
        aiUserSignIn.setIsDelete(0);
        return aiUserSignInMapper.select(aiUserSignIn);
    }

    public int insertSelective(AiUserSignIn aiUserSignIn){
        int insert = aiUserSignInMapper.insertSelective(aiUserSignIn);
        return insert;
    }

    public int updateByPrimaryKeySelective(AiUserSignIn aiUserSignIn){
        int update = aiUserSignInMapper.updateByPrimaryKeySelective(aiUserSignIn);
        return update;
    }


}
