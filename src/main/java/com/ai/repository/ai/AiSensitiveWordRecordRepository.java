package com.ai.repository.ai;

import com.ai.dao.entity.ai.AiSensitiveWordRecord;
import com.ai.dao.mapper.ai.AiSensitiveWordRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/10 19:56
 * @description：
 * @version:
 */
@Repository
public class AiSensitiveWordRecordRepository {

    @Autowired
    private AiSensitiveWordRecordMapper aiSensitiveWordRecordMapper;

    public int insertSelective(AiSensitiveWordRecord aiSensitiveWordRecord){
        int insert = aiSensitiveWordRecordMapper.insertSelective(aiSensitiveWordRecord);
        return insert;
    }
}
