package com.ai.repository.ai;

import com.ai.dao.entity.ai.AiUserAccessRecord;
import com.ai.dao.mapper.ai.AiUserAccessRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AiUserAccessRecordRepository {

    @Autowired
    private AiUserAccessRecordMapper aiUserAccessRecordMapper;

    public int insertSelective(AiUserAccessRecord aiUserAccessRecord){
        int insert = aiUserAccessRecordMapper.insertSelective(aiUserAccessRecord);
        return insert;
    }

    public AiUserAccessRecord getUserCallOnHistory(Long id){
        if (id == null){
            return null;
        }
        AiUserAccessRecord aiUserAccessRecord = aiUserAccessRecordMapper.selectByPrimaryKey(id);
        return aiUserAccessRecord;
    }

    public List<AiUserAccessRecord> getUserCallOnHistory(AiUserAccessRecord aiUserAccessRecord){
        List<AiUserAccessRecord> getUserCallOnHistoryList = aiUserAccessRecordMapper.getUserCallOnHistory(aiUserAccessRecord);
        return getUserCallOnHistoryList;
    }

    public List<AiUserAccessRecord> getUserCallOnHistoryByType(AiUserAccessRecord aiUserAccessRecord){
        List<AiUserAccessRecord> getUserCallOnHistoryList = aiUserAccessRecordMapper.getUserCallOnHistoryByType(aiUserAccessRecord);
        return getUserCallOnHistoryList;
    }

    public int updateByPrimaryKeySelective(AiUserAccessRecord aiUserAccessRecord){
        if (aiUserAccessRecord == null || aiUserAccessRecord.getId() == null){
            return 0;
        }
        int update = aiUserAccessRecordMapper.updateByPrimaryKeySelective(aiUserAccessRecord);
        return update;
    }

}
