package com.ai.repository;

import com.ai.dao.entity.FeedbackSuggest;
import com.ai.dao.mapper.ai.FeedbackSuggestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/27 14:51
 * @description：
 * @version:
 */
@Repository
public class FeedbackSuggestRepository {

    @Autowired
    private FeedbackSuggestMapper feedbackSuggestMapper;

    public int insertSelective(FeedbackSuggest feedbackSuggest) {
        return feedbackSuggestMapper.insertSelective(feedbackSuggest);
    }
}
