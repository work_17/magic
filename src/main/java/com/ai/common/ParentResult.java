package com.ai.common;

import lombok.Data;

@Data
public class ParentResult {
    /**
     * 返回编码:0-成功，非0-失败
     */
    public Integer code = 0;
    /**
     * 失败信息
     */
    public String msg;

}
