package com.ai.common.gpt.openAiRes.text;

import com.ai.common.gpt.openAiRes.text.ChatChoice;
import com.ai.common.gpt.subscriber.Usage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OpenAiResponse implements Serializable {
    private String id;
    private String object;
    private long created;
    private String model;
    private List<ChatChoice> choices;
    private Usage usage;
}
