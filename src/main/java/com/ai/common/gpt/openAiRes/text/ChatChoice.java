package com.ai.common.gpt.openAiRes.text;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ChatChoice implements Serializable {
    private long index;
    @JsonProperty("delta")
    private OpenAiMessage delta;
    @JsonProperty("message")
    private OpenAiMessage message;
    @JsonProperty("finish_reason")
    private String finishReason;
}
