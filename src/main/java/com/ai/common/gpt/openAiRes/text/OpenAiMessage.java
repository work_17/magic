package com.ai.common.gpt.openAiRes.text;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/18 11:37
 * @description：
 * @version:
 */
@Data
public class OpenAiMessage {
    private String role;
    private String content;
}
