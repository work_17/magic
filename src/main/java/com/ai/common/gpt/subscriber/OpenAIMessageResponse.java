package com.ai.common.gpt.subscriber;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/18 11:12
 * @description：
 * @version:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenAIMessageResponse {
    private String content;
    private Boolean isEnd;
}
