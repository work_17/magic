package com.ai.common;

import java.util.*;
import java.util.stream.Collectors;

public class Demo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("eygd");
        list.add("ufh");
        list.add("ufrh");
        list.add("xfh");
        list.add("uh");

        List<String> collect = list.stream().filter(x -> x.length() == 3).collect(Collectors.toList());
        System.out.println(collect);
    }
}
