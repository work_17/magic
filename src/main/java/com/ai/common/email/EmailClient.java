package com.ai.common.email;

import com.ai.common.enums.ConfigModuleEnum;
import com.ai.common.utils.ConfigUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 15:22
 * @description：
 * @version:
 */
@Slf4j
@Component
public class EmailClient {

    @Autowired
    private JavaMailSender jms;

    /**
     * 公共邮件接口
     * @param title
     * @param content
     */
    public void sendEmail(String title,String content){
        if (StringUtils.isBlank(title) || StringUtils.isBlank(content)){
            return;
        }
        String defaultToEmails = ConfigUtil.getInstance().getConfig(ConfigModuleEnum.email.getModule(), "defaultToEmails");
        if (StringUtils.isNotBlank(defaultToEmails)) {
            String[] defaultToEmailsArray = defaultToEmails.split(",");
            List<String> toEmails = new ArrayList<>(defaultToEmailsArray.length);
            //数组转list
            Collections.addAll(toEmails, defaultToEmailsArray);
            send(title, content,toEmails, null, null, null);
        }
    }

    public void send(String subject,String content,List<String> toEmails){
        send(subject, content,toEmails, null, null, null);
    }

    public void send(String subject,String content,List<String> toEmails, Boolean html){
        send(subject,content, toEmails, null, null, html);
    }

    public void send(String subject,String content, List<String> toEmails, List<String> ccEmails, List<String> bccEmails, Boolean html){
        if (StringUtils.isBlank(subject) || StringUtils.isBlank(content) || CollectionUtils.isEmpty(toEmails)){
            return;
        }
        try {
            String[] sMail = toEmails.toArray(new String[]{});
            String[] ccMail = null;
            String[] bccMail = null;
            if (CollectionUtils.isNotEmpty(ccEmails)) {
                ccMail = ccEmails.toArray(new String[]{});
            }
            if (CollectionUtils.isNotEmpty(bccEmails)) {
                bccMail = bccEmails.toArray(new String[]{});
            }
            MimeMessage mimeMessage = jms.createMimeMessage();
            //注意这里的boolean,等于真的时候才能嵌套图片，在构建MimeMessageHelper时候，所给定的值是true表示启用，
            //multipart模式
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            //发送者
            messageHelper.setFrom("969499247@qq.com");
            //接收者
            messageHelper.setTo(sMail);
            //抄送
            if (ccMail != null) {
                messageHelper.setCc(ccMail);
            }
            //密送
            if (bccMail != null) {
                messageHelper.setBcc(bccMail);
            }
            //发送的标题
            messageHelper.setSubject(subject);
//            //发送的html内容
//            String text = "<html><head></head><body><h1>hello!!spring html Mail</h1><p style='color:#F00'>红色字</p></body></html>";
            //true表示解析html
            messageHelper.setText(content, html != null);
            //附件
//            FileSystemResource img = new FileSystemResource(new File("D:/data/abc.jpg"));
//            messageHelper.addAttachment("image.jpg", img);
//            FileSystemResource file = new FileSystemResource(new File("D:/test/邮件附件测试.txt"));
//            messageHelper.addAttachment("测试文档.txt", file);
            jms.send(mimeMessage);
        }catch (Exception e){
            log.error(e.getMessage(),e);
        }

    }
}
