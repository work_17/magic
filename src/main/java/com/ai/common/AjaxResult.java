package com.ai.common;



import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

/**
 * 操作消息提醒
 *
 * @author wirror800
 */
public class AjaxResult extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    public static final String CODE_TAG = "code";

    /**
     * 返回内容
     */
    public static final String MESSAGE_TAG = "msg";

    /**
     * 数据对象
     */
    public static final String RESULT_TAG = "result";

    /**
     * 数据对象
     */
    public static final String TYPE_TAG = "type";

    /**
     * 数据对象
     */
    public static final String TIME_TAG = "timestamp";

    /**
     * 通用错误码
     */
    public static final int COMMON_ERROR_CODE = 500;

    /**
     * 成功码
     */
    public static final int SUCCESS_CODE = 0;


    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public AjaxResult() {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param message  返回内容
     */
    public AjaxResult(int code, String message) {
        super.put(TYPE_TAG, code == 0 ? "success" : "error");
        super.put(CODE_TAG, code);
        super.put(MESSAGE_TAG, message);
        super.put(TIME_TAG, (int) System.currentTimeMillis() / 1000);
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param message  返回内容
     * @param data 数据对象
     */
    public AjaxResult(int code, String message, Object data) {
        super.put(TYPE_TAG, code == 0 ? "success" : "error");
        super.put(CODE_TAG, code);
        super.put(MESSAGE_TAG, message);
        super.put(TIME_TAG, System.currentTimeMillis() / 1000);
        if (data != null) {
            super.put(RESULT_TAG, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static AjaxResult success() {
        return AjaxResult.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static AjaxResult success(Object data) {
        return AjaxResult.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param message 返回内容
     * @return 成功消息
     */
    public static AjaxResult success(String message) {
        return AjaxResult.success(message, null);
    }

    /**
     * 返回成功消息
     *
     * @param message  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static AjaxResult success(String message, Object data) {
        return new AjaxResult(SUCCESS_CODE, message, data);
    }

    /**
     * 返回错误消息
     *
     * @return
     */
    public static AjaxResult error() {
        return AjaxResult.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param message 返回内容
     * @return 警告消息
     */
    public static AjaxResult error(String message) {
        return AjaxResult.error(message, null);
    }

    /**
     * 返回错误消息
     *
     * @param message  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static AjaxResult error(String message, Object data) {
        return new AjaxResult(COMMON_ERROR_CODE, message, data);
    }

    /**
     * 带错误码带数据data的返回
     * @param message 提示信息
     * @param code 错误码
     * @param data 数据
     * @return
     */
    public static AjaxResult error(String message, int code, Object data) {
        return new AjaxResult(code, message, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param message  返回内容
     * @return 警告消息
     */
    public static AjaxResult error(int code, String message) {
        return new AjaxResult(code, message, null);
    }
}
