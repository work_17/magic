package com.ai.common.http.dto;

import lombok.Data;

@Data
public class FluxMessage {
    private String content;
    private Boolean isEnd;

    public FluxMessage() {
    }

    public FluxMessage(String content, Boolean isEnd) {
        this.content = content;
        this.isEnd = isEnd;
    }
}
