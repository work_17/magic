package com.ai.common.http.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/10 11:13
 * @description：
 * @version:
 */
@Data
public class WxMsgSecCheckResultDto {
    private String suggest;
    private Integer label;
}
