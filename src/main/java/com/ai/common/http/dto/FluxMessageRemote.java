package com.ai.common.http.dto;

import lombok.Data;

@Data
public class FluxMessageRemote {
    private String msg;
    private Boolean isEnd;

    public FluxMessageRemote() {
    }

    public FluxMessageRemote(String msg, Boolean isEnd) {
        this.msg = msg;
        this.isEnd = isEnd;
    }
}
