package com.ai.common.http.dto;

import lombok.Data;

@Data
public class WXOpenIdResponse {
    private String session_key;
    private String unionid;
    private String errmsg;
    private String openid;
    private Integer errcode;
}
