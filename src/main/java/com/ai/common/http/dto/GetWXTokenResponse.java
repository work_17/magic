package com.ai.common.http.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/10 10:55
 * @description：
 * @version:
 */
@Data
public class GetWXTokenResponse {
    /**
     * 获取到的凭证
     */
    private String access_token;
    /**
     * 凭证有效时间，单位：秒
     */
    private Integer expires_in;

    /**
     * 0或null为返回成功
     */
    private Integer errcode;

    private String errmsg;
}
