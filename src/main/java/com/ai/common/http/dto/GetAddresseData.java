package com.ai.common.http.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 16:36
 * @description：
 * @version:
 */
@Data
public class GetAddresseData {
    private String location;
}
