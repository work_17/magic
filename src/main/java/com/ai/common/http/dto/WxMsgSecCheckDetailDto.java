package com.ai.common.http.dto;

import lombok.Data;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/10 11:13
 * @description：
 * @version:
 */
@Data
public class WxMsgSecCheckDetailDto {
    private String strategy;
    private String keyword;
    private Integer errcode;
    private String suggest;
    private Integer label;
    private Integer prob;
}
