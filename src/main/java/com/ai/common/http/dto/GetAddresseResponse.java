package com.ai.common.http.dto;

import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 16:36
 * @description：
 * @version:
 */
@Data
public class GetAddresseResponse {
    List<GetAddresseData> data;
}
