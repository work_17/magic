package com.ai.common.http.dto;

import lombok.Data;

import java.util.List;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/5/10 11:10
 * @description：
 * @version:
 */
@Data
public class WxMsgSecCheckResponse {
    /**
     * 0为返回成功
     */
    private Integer errcode;

    private String errmsg;

    private String trace_id;

    private WxMsgSecCheckResultDto result;

    private List<WxMsgSecCheckDetailDto> detail;
}
