package com.ai.common.http;

import com.ai.sevice.config.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class WebClient {


    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ConfigService configService;

    public String get(String url){
        if (StringUtils.isBlank(url)){
            return null;
        }
        //log.info("webClientGetRequest -> " + url);
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
        //log.info("webClientGetResponse -> " + forEntity.getBody());
        return forEntity.getBody();
    }
}
