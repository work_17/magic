package com.ai.common.enums;

/**
 * 配置模块枚举
 * @author ：shengwenyang
 * @date ：Created in 2023/4/24 16:45
 * @description：
 * @version:
 */
public enum ConfigModuleEnum {
    /**
     * 默认模块
     */
    DEFAULT("default", "默认配置"),
    /**
     * 卡片列表
     */
    aiCard("aiCard", "卡片列表配置"),
    /**
     * ai请求示例
     */
    aiExample("aiExample", "ai请求示例配置"),
    /**
     * ai接口请求内容组装
     */
    aiAssembly("aiAssembly", "ai接口请求内容组装配置"),
    /**
     * 邮箱
     */
    email("email", "邮箱配置"),
    /**
     *积分
     */
    integral("integral", "积分配置"),
    /**
     * 分享
     */
    share("share", "分享配置"),
    /**
     * 股票
     */
    stock("stock", "股票配置"),
    /**
     * 广告
     */
    advertising("advertising", "广告配置"),
    ;
    private String module;
    private String desc;

    ConfigModuleEnum(String module, String desc) {
        this.module = module;
        this.desc = desc;
    }

    public String getModule() {
        return module;
    }

    public String getDesc() {
        return desc;
    }
}
