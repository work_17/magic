package com.ai.common.enums;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/20 19:06
 * @description：
 * @version:
 */
public enum QAEnum {

    question(0),//问题

    answer(1);//答案

    private Integer value;

    QAEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static QAEnum valueOf(Integer value) {
        if(value == null){
            return question;
        }
        for (QAEnum item : QAEnum.values()) {
            if (value.equals(item.getValue())) {
                return item;
            }
        }
        return question;
    }
}
