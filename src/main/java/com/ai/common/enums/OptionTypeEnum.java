package com.ai.common.enums;

import org.apache.commons.lang3.StringUtils;

public enum OptionTypeEnum {
    DEFAULT(""),
    /**
     * 聊天
     */
    chat("chat"),
    /**
     * 美食
     */
    food("food"),
    /**
     * 推广文案
     */
    promotion("promotion"),
    /**
     * 问答助手
     */
    faq("faq"),
    /**
     * 编程
     */
    programmer("programmer"),
    /**
     * 实时翻译
     */
    translate("translate"),
    /**
     * 智能作家
     */
    composition("composition"),
    /**
     * 诗词
     */
    poetry("poetry"),
    /**
     * 文言文
     */
    classical("classical"),
    /**
     * 邮件
     */
    eMail("eMail"),
    /**
     * 日报
     */
    daily("daily"),
    /**
     * 周报
     */
    weekly("weekly"),
    /**
     * 工作总结
     */
    workSummary("workSummary"),
    /**
     * 音乐
     */
    music("music"),
    /**
     * 视频
     */
    video("video"),
    ;
    private String optionType;

    OptionTypeEnum(String optionType) {
        this.optionType = optionType;
    }

    public String getValue() {
        return optionType;
    }

    public static OptionTypeEnum get(String optionType) {
        if(StringUtils.isBlank(optionType)){
            return DEFAULT;
        }
        for (OptionTypeEnum item : OptionTypeEnum.values()) {
            if (optionType.equals(item.getValue())) {
                return item;
            }
        }
        return DEFAULT;
    }
}
