package com.ai.common.enums;

public enum SignType {
    HMACSHA256("HMAC-SHA256"),
    MD5("MD5"),
    RSA("RSA");

    private final String type;

    private SignType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public String toString() {
        return this.type;
    }
}
