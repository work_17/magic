package com.ai.common;

public final class RefObject<T> {
    private T argvalue;

    public T getArgvalue() {
        return argvalue;
    }

    public void setArgvalue(T argvalue) {
        this.argvalue = argvalue;
    }

    public RefObject(T refarg)
    {
        argvalue = refarg;
    }
}
