package com.ai.common.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * https://github.com/redisson/redisson/wiki/%E7%9B%AE%E5%BD%95
 */
@Configuration
public class RedissonConfig {
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://124.222.159.231:6379")
                .setPassword("ysw123456");
        return Redisson.create(config);
    }
}
