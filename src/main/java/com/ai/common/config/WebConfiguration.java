//package com.ai.common.config;
//
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.converter.StringHttpMessageConverter;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.util.ResourceUtils;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//
//import java.nio.charset.Charset;
//import java.nio.charset.StandardCharsets;
//import java.util.List;
//
//@Configuration
//public class WebConfiguration extends WebMvcConfigurationSupport {
//
//    /**
//     * 配置静态资源处理程序
//     * @param registry
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//        //addResourceHandler是指你想在url请求的路径
//        //addResourceLocations是图片存放的真实路径
////        String filePath = "file:D:/upload/";
//        String filePath = "file:/usr/local/metal/images/";
//        //所以当需要访问上传的图片时，url路径为：http://你的服务器IP:8080/images/1.jpg
//        registry.addResourceHandler("/images/**").addResourceLocations(filePath);
//        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
//        //registry.addResourceHandler("/**").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX + "/static/");
//        super.addResourceHandlers(registry);
//
//    }
//
//    /**
//     * 配置视图解析器
//     * @param registry
//     */
//    @Override
//    protected void configureViewResolvers(ViewResolverRegistry registry) {
//        registry.jsp("/", ".html");
//    }
//
////    /**
////     * http请求时编码
////     */
////    @Bean
////    public HttpMessageConverter<String> responseBodyConverter() {
////        StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
////        return converter;
////    }
////
////    /**
////     * 系统配置参数编码
////     * @param converters
////     */
////    @Override
////    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
////
////        for (HttpMessageConverter<?> converter :converters){
////            if(converter instanceof StringHttpMessageConverter){
////                ((StringHttpMessageConverter)converter).setDefaultCharset(StandardCharsets.UTF_8); //将StringHttpMessageConverter改为utf-8
////            }
////        }
////        super.extendMessageConverters(converters);
////
//////        if (converters.size() > 0) {
//////            converters.add(converters.get(0));
//////            converters.set(0, responseBodyConverter()); // 放到第一位
//////        } else {
//////            converters.add(responseBodyConverter());
//////        }
////
////
//////        super.configureMessageConverters(converters);
//////        converters.add(responseBodyConverter());
////    }
//    @Override
//    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
//        //参考 http://www.manongjc.com/detail/59-gzwgkonxienxjnr.html
//        for (HttpMessageConverter<?> converter : converters) {
//            // 解决 Controller 返回普通文本中文乱码问题
//            if (converter instanceof StringHttpMessageConverter) {
//                ((StringHttpMessageConverter) converter).setDefaultCharset(StandardCharsets.UTF_8);
//            }
//
//            // 解决 Controller 返回json对象中文乱码问题
//            if (converter instanceof MappingJackson2HttpMessageConverter) {
//                ((MappingJackson2HttpMessageConverter) converter).setDefaultCharset(StandardCharsets.UTF_8);
//            }
//        }
//    }
//}
