package com.ai.common.utils;

import com.ai.common.enums.ConfigModuleEnum;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class TokenUtil {

    ///////////////////////有色金属////////////////////////////////////////////////////////////////////
    private static final String METAL_DEFAULT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjZWxscGhvbmUiOiIxODcyMTEyODI1MyIsImNvbXBhbnlfaWQiOjAsImNvbXBhbnlfc3RhdHVzIjowLCJjcmVhdGVfYXQiOjE2ODE0MzcyODgsImVtYWlsIjoiIiwiZW5fZW5kX3RpbWUiOjAsImVuX3JlZ2lzdGVyX3N0ZXAiOjEsImVuX3JlZ2lzdGVyX3RpbWUiOjAsImVuX3N0YXJ0X3RpbWUiOjAsImVuX3VzZXJfdHlwZSI6MCwiZW5kX3RpbWUiOjAsImlzX21haWwiOjAsImlzX3Bob25lIjowLCJsYW5ndWFnZSI6IiIsImx5X2VuZF90aW1lIjowLCJseV9zdGFydF90aW1lIjowLCJseV91c2VyX3R5cGUiOjAsInJlZ2lzdGVyX3RpbWUiOjE2NDYyMTI0NjIsInN0YXJ0X3RpbWUiOjAsInVuaXF1ZV9pZCI6IjVhOTY3OTFjM2ZmYzczYzg0NTU3MDY2MDc4Y2NhMWE0IiwidXNlcl9pZCI6MjY1ODU3MCwidXNlcl9sYW5ndWFnZSI6ImNuIiwidXNlcl9uYW1lIjoiU01NMTY0NjIxMjQ2M0xwIiwidXNlcl90eXBlIjowLCJ1dWlkX3NoYTI1NiI6IjZlMGM5Zjc4MGU1YzNmZTVlMDZmNDViMjQwNzQzN2NjYzc5YzkyNjUyYjUyZTU3MTM5ZGViYjlmYTQwZWEyOGMiLCJ6eF9lbmRfdGltZSI6MCwienhfc3RhcnRfdGltZSI6MCwienhfdXNlcl90eXBlIjowfQ.X9uJ1o4aTsjZ63ItJdl3MqsDg-Kik7lnGq2lQkTsctw";

    private static String METAL_TOKEN = METAL_DEFAULT;

    public static String getMetalToken() {
        return ConfigUtil.getInstance().getConfig(ConfigModuleEnum.DEFAULT.getModule(),"metalToken");
    }

    public static void setMetalToken(String token) {
        if (token == null){
            METAL_TOKEN = METAL_DEFAULT;
            return;
        }
        METAL_TOKEN = token;
    }
    ///////////////////////chatGPT////////////////////////////////////////////////////////////////////
    private static String TOKEN1 = "Bearer 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";
    private static String TOKEN2 = "Bearer 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";
    private static String TOKEN3 = "Bearer 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";
    private static String TOKEN4 = "Bearer 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";
    private static String TOKEN5 = "Bearer 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";
    private static String TOKEN6 = "Bearer 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";

    private static int FLAG = 0;

    //轮询获取
    public static String pollChatGptToken() {
        String tokens = ConfigUtil.getInstance().getConfig(ConfigModuleEnum.DEFAULT.getModule(), "tokens");
        if (StringUtils.isBlank(tokens)){
            return null;
        }
        List<String> tokenList = JSON.parseArray(tokens,String.class);
        if (CollectionUtils.isEmpty(tokenList)){
            return null;
        }
        if (FLAG >= tokenList.size()){
            FLAG = 0;
        }
        String token = tokenList.get(FLAG);
        FLAG ++;
        return token;
    }

    public static String getXueQiuCookie() {
        return ConfigUtil.getInstance().getConfig(ConfigModuleEnum.DEFAULT.getModule(),"xueQiuCookie");
    }
}
