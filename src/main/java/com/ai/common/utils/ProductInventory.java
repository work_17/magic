package com.ai.common.utils;

import java.util.Random;

public class ProductInventory {

    private static int aInventory = 30;
    private static int bInventory = 20;
    private static int cInventory = 10;

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            String product = getRandomProduct();
            System.out.println("拿出的产品是：" + product);
            System.out.println();
        }
    }

    public static String getRandomProduct() {

        Random random = new Random();
        int randomNumber = random.nextInt(aInventory + bInventory + cInventory);

        if (randomNumber < aInventory) {
            aInventory--;
            return "A";
        } else if (randomNumber < aInventory + bInventory) {
            bInventory--;
            return "B";
        } else {
            cInventory--;
            return "C";
        }
    }
}
