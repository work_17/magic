package com.ai.common.utils;

import org.apache.commons.lang3.math.NumberUtils;

public class StrUtil {

    private final static String x = "";

    //获取第一个非数字字符串
    public static String getFirstNonNumericStr(String str){
        int index = str.indexOf(")");
        String substring = str.substring(0, index + 1);
        return substring;
    }


    public static void main(String[] args) {
        String str1 = "(升)70~(升)110";
        String str = "70~110";
//        System.out.println(getPriceUnit(str));
//        System.out.println(getNumericPrice(str));
//        System.out.println(isNonNumeric(str1));

        boolean a = NumberUtils.isNumber("aaa");
        boolean b = NumberUtils.isNumber("12.3");
        boolean c = NumberUtils.isNumber("-95");
        boolean d = NumberUtils.isNumber(null);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);

    }
}
