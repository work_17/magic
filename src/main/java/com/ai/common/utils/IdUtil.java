package com.ai.common.utils;


public class IdUtil {

    private static final String STRING_POOL = "abcdefghijklmnopqrstuvwxyz0123456789";

    /**
     * 获取指定位数的随机字符串(包含小写字母、数字)
     */
    public static String getRandomString(int length) {
        //随机字符串的随机字符库
        StringBuilder sb = new StringBuilder();
        int len = STRING_POOL.length();
        for (int i = 0; i < length; i++) {
            sb.append(STRING_POOL.charAt((int) Math.round(Math.random() * (len - 1))));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(getRandomString(8));
    }
}
