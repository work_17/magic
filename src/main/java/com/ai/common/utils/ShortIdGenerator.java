package com.ai.common.utils;

import java.util.UUID;

public class ShortIdGenerator {
    public static String generateShortId() {
        // 生成UUID
        UUID uuid = UUID.randomUUID();

        // 将UUID转换为字符串
        String uuidString = uuid.toString();

        // 去掉UUID中的"-"
        String shortId = uuidString.replace("-", "");

        System.out.println(uuidString);
        System.out.println(shortId);
        // 截取字符串的前8位作为用户id
        shortId = shortId.substring(0, 8);
        return shortId;
    }

    public static void main(String[] args) {
        String shortId = generateShortId();
        System.out.println("Short Id: " + shortId);
    }
}
