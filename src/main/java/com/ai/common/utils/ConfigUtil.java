package com.ai.common.utils;

import com.ai.common.enums.ConfigModuleEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:49
 * @description：
 * @version:
 */
public class ConfigUtil {

    //构造方法私有化
    private ConfigUtil() {}
    private static final ConfigUtil single = new ConfigUtil();
    //静态方法
    public static ConfigUtil getInstance() {
        return single;
    }

    private ConcurrentHashMap<String,ConcurrentHashMap<String,String>> configModuleMap = new ConcurrentHashMap<>();

    private ConcurrentHashMap<String,String> optionTypeMap = new ConcurrentHashMap<>();

    /**
     * 获取全部配置
     */
    public ConcurrentHashMap<String,ConcurrentHashMap<String,String>> getConfigModuleMap(){
        return this.configModuleMap;
    }

    /**
     * 重置配置
     * @param configModuleMap
     */
    public void resetConfig(ConcurrentHashMap<String,ConcurrentHashMap<String,String>> configModuleMap){
        this.configModuleMap = configModuleMap;
    }

    /**
     * 重置optionTypeMap
     * @param optionTypeMap
     */
    public void resetOptionTypeMap(ConcurrentHashMap<String,String> optionTypeMap){
        this.optionTypeMap = optionTypeMap;
    }

    /**
     * 获取optionTypeMap
     */
    public ConcurrentHashMap<String,String> getOptionTypeMap(){
        return this.optionTypeMap;
    }

    /**
     * 获取optionType
     */
    public String getOptionType(String optionType){
        return this.optionTypeMap.get(optionType);
    }

    /**
     * 获取配置信息
     * @param module
     * @param key
     * @return
     */
    public String getConfig(String module,String key){
        if (StringUtils.isBlank(module) || StringUtils.isBlank(key)){
            return null;
        }
        ConcurrentHashMap<String, String> configMap = configModuleMap.get(module);
        if (configMap == null){
            return null;
        }
        return configMap.get(key);
    }

    /**
     * 获取配置信息
     * @param module
     * @param key
     * @return
     */
    public List<String> getConfigList(String module, String key){
        if (StringUtils.isBlank(module) || StringUtils.isBlank(key)){
            return null;
        }
        ConcurrentHashMap<String, String> configMap = configModuleMap.get(module);
        if (configMap == null){
            return null;
        }
        String valueStr = configMap.get(key);
        if (StringUtils.isNotBlank(valueStr)) {
            String[] valueArr = valueStr.split(",");
            List<String> valueList = new ArrayList<>(valueArr.length);
            //数组转list
            Collections.addAll(valueList, valueArr);
            return valueList;
        }
        return null;
    }

    /**
     * 新增或更新配置
     * @param module
     * @param key
     * @return
     */
    public boolean addOrUpdateConfig(String module,String key,String value){
        if (StringUtils.isBlank(module) || StringUtils.isBlank(key)){
            return false;
        }
        ConcurrentHashMap<String, String> configMap = configModuleMap.get(module);
        if (configMap != null){
            //更新
            configMap.put(key,value);
        }else{
            //新增
            configMap = new ConcurrentHashMap<>();
            configMap.put(key,value);
            configModuleMap.put(module,configMap);
        }
        return true;
    }

    /**
     * 删除配置
     * @param module
     * @param key
     * @return
     */
    public boolean deleteConfig(String module,String key){
        if (StringUtils.isBlank(module) || StringUtils.isBlank(key)){
            return false;
        }
        ConcurrentHashMap<String, String> configMap = configModuleMap.get(module);
        if (configMap != null){
            String value = configMap.get(key);
            if (value != null){
                configMap.remove(key);
                return true;
            }
        }
        return false;
    }

    public boolean isShowCard(String uid){
        //设置卡片白名单
        String cardWhiteListStr = ConfigUtil.getInstance().getConfig(ConfigModuleEnum.aiCard.getModule(), "cardWhiteList");
        if (StringUtils.isNotBlank(cardWhiteListStr)) {
            List<String> cardWhiteListSList = Arrays.asList(cardWhiteListStr.split(","));
            if (cardWhiteListSList.contains(uid)){
                return true;
            }
        }
        return false;
    }

}
