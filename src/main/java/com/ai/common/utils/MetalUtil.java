package com.ai.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;

public class MetalUtil {
    //获取价格单位
    public static String getPriceUnit(String str){
        if (StringUtils.isBlank(str)){
            return null;
        }
        str = StrUtil.getFirstNonNumericStr(str);
        if (StringUtils.isBlank(str)){
            return null;
        }
        str = str.replace("(","").replace(")","");
        return str;
    }

    //获取数字价格
    public static String getNumericPrice(String str){
        if (StringUtils.isBlank(str)){
            return null;
        }
        String firstNonNumericStr = StrUtil.getFirstNonNumericStr(str);
        if (StringUtils.isBlank(firstNonNumericStr)){
            return null;
        }
        str = str.replace(firstNonNumericStr,"");
        return str;
    }

    //判断是否是数字
    public static boolean isNonNumeric(String str){
        if (StringUtils.isBlank(str)){
            return false;
        }
        return !str.contains("(");
    }

    //获取数值类型数据
    public static BigDecimal getNumber(String numberStr){
        return NumberUtils.isNumber(numberStr) ? new BigDecimal(numberStr) : null;
    }

    public static void main(String[] args) {
        String str1 = "(升)70~(升)110";
        String str = "70~110";
        System.out.println(getPriceUnit(str));
        System.out.println(getNumericPrice(str));
        System.out.println(isNonNumeric(str1));


        String x = null;
        String y = x + ",aa";
        y = y.replace("null", "");
        System.out.println(y);

    }
}
