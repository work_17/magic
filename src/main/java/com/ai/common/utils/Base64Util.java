package com.ai.common.utils;

import org.apache.commons.codec.binary.Base64;

public class Base64Util {

    private static final Base64 base64;

    public static final byte[] LINE_SEPARATOR = {};

    static {
        base64 = new Base64(256, LINE_SEPARATOR, true);
    }

    /**
     * 功能：编码字符串
     */
    public static String encode(String data) {
        return encode(data.getBytes());
    }

    /**
     * BASE64字符串解码为二进制数据
     * @param source
     * @return
     * @throws Exception
     */
    public static byte[] decode(String source)  {
        return base64.decode(source.getBytes());
    }

    /**
     * 二进制数据编码为BASE64字符串
     * @param source
     * @return
     * @throws Exception
     */
    public static String encode(byte[] source)  {
        return new String(base64.encode(source));
    }

    public static String base64Encode(String source){
        return new String(base64.encode(source.getBytes()));
    }

    public static String base64Encode(byte[] source){
        return new String(base64.encode(source));
    }

    public static String base64Decode(String source){
        return new String(base64.decode(source));
    }

    public static byte[] base64DecodeToByte(String source){
        return base64.decode(source);
    }

}
