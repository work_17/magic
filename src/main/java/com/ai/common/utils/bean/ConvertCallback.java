package com.ai.common.utils.bean;

@FunctionalInterface
public interface ConvertCallback<T, S>  {

    void callback(S source ,T target);

}