package com.ai.common.utils;

import org.apache.commons.lang3.StringUtils;

public class PhoneUtil {

    private static final String aesKey = "Shanghai@#Zyq#12";

    /**
     * 手机号加密
     * @param phone
     * @return
     */
    public static String encrypt(String phone) {
        if (StringUtils.isBlank(phone)){
            return null;
        }
        String phone1 = "";
        String phone2 = "";
        String phone3 = "";
        if (phone.length() <= 3){
            return null;
        }else if (phone.length() > 3 && phone.length() <= 7){
            phone1 = phone.substring(0,3);
            phone2 = phone.substring(3);
        }else{
            phone1 = phone.substring(0,3);
            phone2 = phone.substring(3, 7);
            phone3 = phone.substring(7);
        }
        String encryptCBC = AESUtil.encryptCBC(phone2, aesKey);
        return phone1 + encryptCBC + phone3;
    }

    public static void main(String[] args) {
        String phoneNumber = "13812345678";
        String middlePart1 = phoneNumber.substring(0,3);
        String middlePart2 = phoneNumber.substring(3, 7);
        String middlePart3 = phoneNumber.substring(7);
        System.out.println("手机号前面的部分是：" + middlePart1);
        System.out.println("手机号中间的部分是：" + middlePart2);
        System.out.println("手机号后面的部分是：" + middlePart3);
        System.out.println("手机号加密后是：" + encrypt("13812345678"));
    }



}
