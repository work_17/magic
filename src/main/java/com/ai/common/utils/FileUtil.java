package com.ai.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author yangshengwen
 */
public class FileUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 读取文本文件
     *
     * @param path 文件路径
     * @param num  从第num行开始读取
     * @return List<String>
     */
    public static List<String> readLines(String path, int num) {

        List<String> list = new ArrayList<>();

        try (FileReader fr = new FileReader(path); BufferedReader br = new BufferedReader(fr)) {
            //从第num行开始读取
            while (num-- > 1) {
                br.readLine();
            }
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static void main(String[] args) {
        List<String> strings = FileUtil.readLines("D:\\Users\\shengwenyang\\Pictures\\xxx.txt", 1);
        for (String s : strings) {
            String[] split = s.split("：");
            System.out.println(split[0] + ":'" + split[1] + "',");
        }
    }

    /**
     * 得到目录下的所有文件
     *
     * @param path
     * @param isGetInner 是否获取子目录中的文件
     * @return File[]
     */
    public static File[] getFileArray(String path, boolean isGetInner) {

        File file = new File(path);
        // 用于获取当前目录中的文件（子目录的文件由list先装载）
        File[] fileArray = null;
        // 用于装载已经获取到的文件
        List<File> allFile = new ArrayList<>();
        // 如果文件存在的话，不存在只会返回一个空的文件数组
        if (file.exists()) {
            // 文件必须为目录，如果不是目录，也只会有一个空的数组
            if (file.isDirectory()) {
                // 是否获取子目录文件参数判断
                if (isGetInner) {
                    fileArray = file.listFiles();
                    if (null != fileArray) {
                        for (File children : fileArray) {
                            if (children.isDirectory()) {
                                // 如果是目录，而且是获取子目录的参数，那么递归获取子目录中的文件
                                allFile.addAll(Arrays.asList(getFileArray(children.getPath(), true)));
                            } else {
                                allFile.add(children);
                            }
                        }
                    }
                } else {
                    // 不读取子文件目录（将非目录文件放进文件集合中）
                    fileArray = file.listFiles();
                    for (File children : fileArray) {
                        if (children.isFile()) {
                            allFile.add(children);
                        }
                    }
                }
            }
        } else {
            // 如果目录不存在创建目录
            file.mkdirs();
        }
        // 将文件集合转成数组返回
        File[] fileArray2 = new File[allFile.size()];
        return allFile.toArray(fileArray2);
    }

    /**
     * 删除文件
     */
    public static boolean deleteFile(String path) {

        File file = new File(path);

        if (file.exists()) {
            if (file.isFile()) {
                return file.delete();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 获取文件的字节
     *
     * @param file
     * @return
     * @Description:
     */
    public static byte[] getByte(File file) {

        byte[] ret = null;
        try {
            if (file == null) {
                return null;
            }
            FileInputStream in = new FileInputStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream(4096);
            byte[] b = new byte[4096];
            int n;
            while ((n = in.read(b)) != -1) {
                out.write(b, 0, n);
            }
            in.close();
            out.close();
            ret = out.toByteArray();
        } catch (IOException e) {
            logger.error("helper:get bytes from file process error!");
        }
        return ret;
    }

    /**
     * 根据目录和文件名生成文件
     *
     * @param path
     * @param fileName
     * @param content
     * @throws IOException
     * @Description
     * @Author xiaozhou.zhou
     * @Modifier xiaozhou.zhou
     */
    public static void createFile(String path, String fileName, String content) throws IOException {

        // 目录生成b
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // 文件生成
        File file = new File(path + "/" + fileName);
        if (content != null) {
            FileOutputStream fos = new FileOutputStream(file);
            byte[] byteArray = content.getBytes();
            ByteArrayInputStream in = new ByteArrayInputStream(byteArray);
            byte[] buffer = new byte[1024];
            int byteread = 0;
            while ((byteread = in.read(buffer)) != -1) {
                // 文件写操作
                fos.write(buffer, 0, byteread);
            }
            fos.flush();
            fos.close();
        } else {
            file.createNewFile();
        }
    }

    /**
     * 创建文件夹
     */
    public static void createFolder(String path) {

        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    /**
     * 写入文件
     *
     * @param file
     * @param message
     */
    public static void writeToTxt(String file, String message) {
        writeToTxt(file, Collections.singletonList(message));
    }

    /**
     * 批量写入文件
     *
     * @param file
     * @param messageList
     */
    public static void writeToTxt(String file, List<String> messageList) {
        //FileWriter写入追加,默认为false，需要改成true
        try (FileWriter writer = new FileWriter(file, true)) {
            // 逐行写入
            for (String message : messageList) {
                writer.write(message + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*public static void writeToTxt(String file, List<String> messageList) {
        try (FileOutputStream fos = new FileOutputStream(file);PrintWriter pw = new PrintWriter(fos)){
            // 逐行写入
            for (String message : messageList) {
                pw.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}