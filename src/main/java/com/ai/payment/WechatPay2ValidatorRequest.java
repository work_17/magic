package com.ai.payment;

import com.wechat.pay.contrib.apache.httpclient.auth.Verifier;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;

@Slf4j
public class WechatPay2ValidatorRequest {
    protected static final long RESPONSE_EXPIRED_MINUTES = 5L;
    protected final Verifier verifier;
    protected final String requestId;

    protected final String body;

    public WechatPay2ValidatorRequest(Verifier verifier, String requestId,String body) {
        this.verifier = verifier;
        this.requestId = requestId;
        this.body = body;
    }

    protected static IllegalArgumentException parameterError(String message, Object... args) {
        message = String.format(message, args);
        return new IllegalArgumentException("parameter error: " + message);
    }

    protected static IllegalArgumentException verifyFail(String message, Object... args) {
        message = String.format(message, args);
        return new IllegalArgumentException("signature verify fail: " + message);
    }

    public final boolean validate(HttpServletRequest request) throws IOException {
        try {
            this.validateParameters(request);
            String message = this.buildMessage(request);
            String serial = request.getHeader("Wechatpay-Serial");
            String signature = request.getHeader("Wechatpay-Signature");
            if (!this.verifier.verify(serial, message.getBytes(StandardCharsets.UTF_8), signature)) {
                throw verifyFail("serial=[%s] message=[%s] sign=[%s], request-id=[%s]", serial, message, signature, requestId);
            } else {
                return true;
            }
        } catch (IllegalArgumentException var5) {
            log.warn(var5.getMessage());
            return false;
        }
    }

    protected final void validateParameters(HttpServletRequest request) {

        String[] headers = new String[]{"Wechatpay-Serial", "Wechatpay-Signature", "Wechatpay-Nonce", "Wechatpay-Timestamp"};
        String header = null;
        String[] var6 = headers;
        int var7 = headers.length;

        for(int var8 = 0; var8 < var7; ++var8) {
            String headerName = var6[var8];
            header = request.getHeader(headerName);
            if (header == null) {
                throw parameterError("empty [%s], request-id=[%s]", headerName, requestId);
            }
        }
        String timestampStr = header;

        try {
            Instant responseTime = Instant.ofEpochSecond(Long.parseLong(timestampStr));
            if (Duration.between(responseTime, Instant.now()).abs().toMinutes() >= 5L) {
                throw parameterError("timestamp=[%s] expires, request-id=[%s]", timestampStr, requestId);
            }
        } catch (NumberFormatException | DateTimeException var10) {
            throw parameterError("invalid timestamp=[%s], request-id=[%s]", timestampStr, requestId);
        }

    }

    protected final String buildMessage(HttpServletRequest request) throws IOException {
        String timestamp = request.getHeader("Wechatpay-Timestamp");
        String nonce = request.getHeader("Wechatpay-Nonce");
//        String body = this.getResponseBody(request);
        return timestamp + "\n" + nonce + "\n" + body + "\n";
    }

//    protected final String getResponseBody(HttpServletRequest request) throws IOException {
//        HttpEntity entity = request.getEntity();
//        return entity != null && entity.isRepeatable() ? EntityUtils.toString(entity) : "";
//    }
}
