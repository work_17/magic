package com.ai.payment;

import com.alibaba.fastjson.JSON;
import com.wechat.pay.contrib.apache.httpclient.auth.Verifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 测试Controller
 * @author ：shengwenyang
 * @date ：Created in 2023/4/12 16:48
 * @description：
 * @version:
 */
@Slf4j
@RestController
@RequestMapping(value = "payment")
@CrossOrigin(origins = "*",maxAge = 3600)
public class PaymentController {

    @Autowired
    private Verifier verifier;


    @PostMapping(value = "wxPayNotify")
    public Map<String,String> wxPayNotify(@RequestBody Map<String,Object> requestMap, HttpServletRequest request) throws IOException {
        log.info("");
        log.info("支付回调开始");
        String body = JSON.toJSONString(requestMap);
        log.info("请求体：" + body);
        String requestId = (String) requestMap.get("id");
        //验证签名
        WechatPay2ValidatorRequest wechatPay2ValidatorRequest = new WechatPay2ValidatorRequest(verifier,requestId,body);
        boolean validate = wechatPay2ValidatorRequest.validate(request);
        log.info("验证签名结果：" + validate);
        log.info("支付回调结束");
        log.info("");
        Map<String,String> response = new HashMap<>();
        response.put("code","SUCCESS");
        response.put("message","成功");
        return response;
    }

    @PostMapping(value = "wxRefundNotify")
    public Map<String,String> wxRefundNotify(@RequestBody Map<String,Object> requestMap, HttpServletRequest request) throws IOException {
        log.info("");
        log.info("退款回调开始");
        String body = JSON.toJSONString(requestMap);
        log.info("请求体：" + body);
        String requestId = (String) requestMap.get("id");
        //验证签名
        WechatPay2ValidatorRequest wechatPay2ValidatorRequest = new WechatPay2ValidatorRequest(verifier,requestId,body);
        boolean validate = wechatPay2ValidatorRequest.validate(request);
        log.info("验证签名结果：" + validate);
        log.info("退款回调结束");
        log.info("");
        Map<String,String> response = new HashMap<>();
        response.put("code","SUCCESS");
        response.put("message","成功");
        return response;
    }

}
