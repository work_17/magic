package com.ai.scheduled.config;

import com.ai.sevice.ai.UserService;
import com.ai.sevice.config.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 配置定时器
 * @author ：shengwenyang
 * @date ：Created in 2023/4/23 17:46
 * @description：
 * @version:
 */
@Slf4j
@Component
@EnableScheduling
public class ConfigHandle {

    @Autowired
    private ConfigService configService;

    @Autowired
    private UserService userService;

    /**
     * 每5分钟同步一次配置
     */
    @Scheduled(cron = "0 */5 * * * ?")
    public void syncConfig(){
        try {
            //log.info("syncConfig start");
            configService.initConfig();
            //log.info("syncConfig end");
        }catch (Exception e){
            log.warn("syncConfigError");
            log.error(e.getMessage(),e);
        }
    }

    /**
     * 发送新增用户量邮件
     * 每天23点59分发送
     */
    @Scheduled(cron = "0 59 23 * * ?")
    public void sendEmailCurDateUser(){
        try {
            userService.sendEmailCurDateUser();
        }catch (Exception e){
            log.warn("sendEmailCurDateUserError");
            log.error(e.getMessage(),e);
        }
    }
}
